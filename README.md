Repo for putting lightweight data and plotting routines from CIT CTN experiment.

---

With each dataset a readme.md file should be placed in each folder providing either a link to the PSL lab elog or some detailed description of the data. (preferably both).

It is also preferable to add a requirements.txt file when python scripts are present with a list of packages (one to each line).  Users can then generate local enviornments and run a simple 'pip install -r requirements.txt' command to get all packages quickly on local machine.


---

This is not a place for putting large >250 MB data files.
