
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34,
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
##### SET THE BELOW PARAMS #####
################################
isCsv = False # Set if reading in .csv filesi
################################

parser = argparse.ArgumentParser(description='Usage: python spectraStitcher.py spectrum_*.txt')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+', help='Reads in many two column txts assuming the first column is frequency and the second is Vrms/rtHz.  Produces a stitched output of the spectra.')
args = parser.parse_args()

curDate = time.strftime("%Y%m%d_%H%M%S")

# Alter the plot directory path and plot filename
pathToData = os.path.dirname(os.path.abspath(args.files[-1].name)) + '/'
stitchedFileName = curDate + '_StitchedSpectrum.txt'
print 'Data is in              ', pathToData
print 'Stitched Spectrum is in ', stitchedFileName 

# Read in data
plotDict = {}
for ii, arg in enumerate(args.files):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

# Put all frequencies together
allFreqs = np.array([])
allSpecs = np.array([])
for ii in np.arange(len(plotDict)):
  allFreqs = np.append(allFreqs, plotDict[ii][:,0])
  allSpecs = np.append(allSpecs, plotDict[ii][:,1])

# Find f = 0 Hz and delete these points they are weird
# awade: this lowest bin basically includes the DC level, for various reasons
#        we discard it because normalizing correctly is difficult and nuanced
#        , choice of windows can change it, for instance. It looks like you 
#        are removing very low freq points, best just to remove the lowest or 
#        a couple of the lowest for a general purpose script, rather than
#        setting a arbitrary frequency cut off.

index = np.argwhere(np.abs(allFreqs) < 0.000001)
allFreqs = np.delete(allFreqs, index)
allSpecs = np.delete(allSpecs, index)

# Sort the frequencies indicies, and use them to sort both allFreqs and allSpecs
indicies = np.argsort(allFreqs)
allFreqs = allFreqs[indicies]
allSpecs = allSpecs[indicies]

# Save the txt
saveTxt = np.vstack((allFreqs, allSpecs)).T
np.savetxt(pathToData + stitchedFileName, saveTxt, header='Frequency [Hz], Spectrum [Vrms/rtHz]')

