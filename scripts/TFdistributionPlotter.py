import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24,
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24,
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt
import argparse
import time
import os

# Nice colors
tableau20 = [(31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138), 
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
 
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)


################################
################################
loglog = True # Set if we would like a loglog plot
semilogx = False

isCsv = False # Set if reading in .csv files

xAxisLabel = 'Frequency [Hz]'
yAxisLabel = 'TF Magnitude'
y2AxisLabel = 'Relative Mag [\%]'
y3AxisLabel = 'Phase [degs]' # Set y2 axis label
y4AxisLabel = 'Relative Phase [degs]'

plotTitle = 'TF Measurement'
plotLeftTitle = 'TF Measurements $\pm$ Uncertainty'
plotRightTitle = 'Relative Meas $\pm$ Uncertainty'
#plotSaveName = 'PLL_OLGTFs'
################################

parser = argparse.ArgumentParser(description='Usage: python TFdistributionPlotter.py ./path/to/data/folderDescription/TF*.txt   This takes three columns of data, frequency, magnitude and phase, from many .txt files and plots them together.  It also finds the standard deviation and plots it as well.  Puts plots in ./path/to/plots/folderDescription/TF.txt. Can handle many TFs, will put in the folder of the last data file included')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+')
parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
parser.add_argument('--ylabel', type=str, nargs=1,  help='If defined, becomes the ylabel of the 1st plot')
parser.add_argument('--y2label', type=str, nargs=1, help='If defined, becomes the ylabel of the 2nd plot')
parser.add_argument('--y3label', type=str, nargs=1, help='If defined, becomes the ylabel of the 3nd plot')
parser.add_argument('--y4label', type=str, nargs=1, help='If defined, becomes the ylabel of the 4nd plot')
parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the suptitle of the plot')
parser.add_argument('--lefttitle', type=str, nargs=1, help='If defined, becomes the title of the 1st plot')
parser.add_argument('--righttitle', type=str, nargs=1, help='If defined, becomes the title of the 2nd plot')
parser.add_argument('--yAxisLimitPercent', '-yP', type=float, nargs=1, help='If defined, becomes the y axis limit of the 2st plot')
parser.add_argument('--yAxisLimitDegrees', '-yD', type=float, nargs=1, help='If defined, becomes the y axis limit of the 4th plot')
parser.add_argument('--tar', action='store_true', help='If set, creates a tar of the data, plots, and this script.  Default is false.')

args = parser.parse_args()

if args.xlabel is not None:
  xAxisLabel = args.xlabel[0]
if args.ylabel is not None:
  yAxisLabel = args.ylabel[0]
if args.y2label is not None:
  y2AxisLabel = args.y2label[0]
if args.title is not None:
  plotTitle = args.title[0]
if args.lefttitle is not None:
  plotLeftTitle = args.lefttitle[0]
if args.righttitle is not None:
  plotRightTitle = args.righttitle[0]
print 'xlabel =  ', xAxisLabel
print 'ylabel =  ', yAxisLabel
print 'y2label = ', y2AxisLabel
print 'title =   ', plotTitle

labels = np.array([])
for ii, tempFile in enumerate(vars(args)['files']):
  if ii == 0:
    labels = np.append(labels, 'TF Sweep')
  else:
    labels = np.append(labels, '')
  #labels = np.append(labels, tempFile.name.replace('_', ' '))
  print tempFile.name

curDateLabel = time.strftime("%b-%d-%Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

pathToData = os.path.dirname(os.path.abspath(tempFile.name)) + '/'
plotSaveName = os.path.basename(tempFile.name).replace('.txt', '') # plotSaveName is always the same as the last .txt file
plotSaveName = curDate + '_' + plotSaveName
print plotSaveName

# Check if equivalent /plots/ directory exists where the /data/ folder is
pathToPlots = pathToData.replace('/data/', '/plots/')
print 'Data is in ', pathToData
print 'Plots will be in ', pathToPlots
if not os.path.exists(pathToPlots):
  os.makedirs(pathToPlots)

plotDict = {}
txtsWithNans = 0
for ii, arg in enumerate(args.files):
  print 'ii = ', ii
  if isCsv == False:
    tempTxt = np.loadtxt(arg)
  else:
    tempTxt = np.loadtxt(arg, delimiter=',')
  if np.sum(np.isnan(tempTxt)) > 0: # if there are nans don't record anything
    print 
    print 'There are nans in ', arg.name
    print 'Not including in plots'
    args.files = np.delete(args.files, ii)
    txtsWithNans += 1
    continue
  else:
    idx = ii - txtsWithNans
    plotDict[idx] = tempTxt
# Find the distribution, assuming all frequency vectors are the same
measNum = len(plotDict)
freq = plotDict[0][:,0]
freqLen = len(freq)

plotLeftTitle = str(measNum) + ' ' + plotLeftTitle

# convert TF into real and imag parts for simple unc prop
# a + ib = z * exp(i * phi)
compTF = np.zeros([measNum, freqLen], dtype=complex)
real = np.zeros([measNum, freqLen])
imag = np.zeros([measNum, freqLen])
dz_da = np.zeros(freqLen)
dz_db = np.zeros(freqLen)
dphi_da = np.zeros(freqLen)
dphi_db = np.zeros(freqLen)
realMean = np.zeros(freqLen)
imagMean = np.zeros(freqLen)
compMean = np.zeros([freqLen], dtype=complex)
for ii in plotDict.keys():
  mag = plotDict[ii][:,1]
  phase = plotDict[ii][:,2] # This value is in degrees >:(
  phase = np.pi/180.0 * phase  # convert to radians
  compTF[ii,:] = mag * np.exp(1j*phase)
  real[ii,:] = np.real(compTF[ii,:])
  imag[ii,:] = np.imag(compTF[ii,:])

for ii in np.arange(freqLen):
  realMean[ii] = np.median(real[:,ii])
  imagMean[ii] = np.median(imag[:,ii])
  compMean[ii] = realMean[ii] + 1j*imagMean[ii]
  
  # Set up derivatives to change back to magnitude and phase basis for plotting
  dz_da[ii]   = realMean[ii] / np.sqrt(realMean[ii]**2 + imagMean[ii]**2)
  dz_db[ii]   = imagMean[ii] / np.sqrt(realMean[ii]**2 + imagMean[ii]**2)
  dphi_da[ii] = -1.0*imagMean[ii] / (realMean[ii]**2 + imagMean[ii]**2)
  dphi_db[ii] = realMean[ii] / (realMean[ii]**2 + imagMean[ii]**2)

covMatrix = np.zeros([freqLen, 2, 2])
basisMatrix = np.zeros([freqLen, 2, 2])
covMatrixMagPhase = np.zeros([freqLen, 2, 2])
#eigvals = np.zeros(freqLen, 2)
#eigvecs = np.zeros(freqLen, 2, 2)

# Define the basis matrix, make the covariance matrix in real and imaginary basis, and change to magnitude and phase
basisMatrix[:,0,0] = dz_da
basisMatrix[:,0,1] = dz_db
basisMatrix[:,1,0] = dphi_da
basisMatrix[:,1,1] = dphi_db
magUnc = np.zeros(freqLen)
phaseUnc = np.zeros(freqLen)
for ii in np.arange(freqLen):
  covMatrix[ii,:,:] = np.cov(np.array( [real[:,ii], imag[:,ii]] ))
  
  curBasisMatrix = basisMatrix[ii,:,:]
  curBasisMatrixTranspose = np.transpose(curBasisMatrix)
  covMatrixMagPhase[ii,:,:] = curBasisMatrix.dot( covMatrix[ii,:,:].dot( curBasisMatrixTranspose ) )
  
  magUnc[ii] = np.sqrt( covMatrixMagPhase[ii,0,0] )
  phaseUnc[ii] = np.sqrt( covMatrixMagPhase[ii,1,1] )
  
#  eigvals, eigvecs = np.linalg.eig(covMatrix[ii,:,:])
#  eigvecs[ii,:] = np.sqrt(eigvecs[ii,:])

# Make da plot
h = plt.figure(figsize=(16,12))
f1 = h.add_subplot(221)
f2 = h.add_subplot(222)
f3 = h.add_subplot(223)
f4 = h.add_subplot(224)
max1Range = -np.inf
min1Range = np.inf
max2Range = -np.inf
min2Range = np.inf
max3Range = -np.inf
min3Range = np.inf
max4Range = -np.inf
min4Range = np.inf

maxDomain = -np.inf
minDomain = np.inf
alphaValue = 0.1
colorKey = 4

for key in plotDict.keys():
  tempFreq = plotDict[key][:,0]
  tempSpec = plotDict[key][:,1]
  tempPhas = np.pi/180.0 * plotDict[key][:,2]
  
  tempRel = tempSpec * np.exp(1j*tempPhas) / compMean
  tempRelS = np.abs(tempRel)
  tempRelP = np.angle(tempRel)
  
  if loglog == True:
    f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)
  elif semilogx == True:
    f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)
  else:
    f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)

  f3.semilogx(tempFreq, 180.0/np.pi * tempPhas, lw=3, color=tableau20[colorKey], alpha=alphaValue)
  
  f2.semilogx(tempFreq, 100.0 * (tempRelS-1.0), lw=3, color=tableau20[colorKey], alpha = alphaValue)
  f4.semilogx(tempFreq, 180.0/np.pi * tempRelP, lw=3, color=tableau20[colorKey], alpha = alphaValue)

  if max(tempSpec) > max1Range:
    max1Range = max(tempSpec)
  if min(tempSpec) < min1Range:
    min1Range = min(tempSpec)

  if max(100.0*(tempRelS-1.0)) > max2Range:
    max2Range = max(100.0*(tempRelS-1.0))
  if min(100.0*(tempRelS-1.0)) < min2Range:
    min2Range = min(100.0*(tempRelS-1.0))

  if max(180.0/np.pi*tempPhas) > max3Range:
    max3Range = max(180.0/np.pi*tempPhas)
  if min(180.0/np.pi*tempPhas) < min3Range:
    min3Range = min(180.0/np.pi*tempPhas)
 
  if max(180.0/np.pi*tempRelP) > max4Range:
    max4Range = max(180.0/np.pi*tempRelP)
  if min(180.0/np.pi*tempRelP) < min4Range:
    min4Range = min(180.0/np.pi*tempRelP)

  if max(tempFreq) > maxDomain:
    maxDomain = max(tempFreq)
  if min(tempFreq) < minDomain:
    minDomain = min(tempFreq)

colorKey = 0
alphaValue = 0.6
if loglog == True:
  f1.loglog(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.loglog(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.loglog(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
elif semilogx == True:
  f1.semilogx(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.semilogx(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.semilogx(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
else:
  f1.plot(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.plot(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.plot(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)

f3.semilogx(tempFreq, 180.0/np.pi * np.angle(compMean), lw=3, color=tableau20[colorKey], alpha=alphaValue)
f3.semilogx(tempFreq, 180.0/np.pi * (np.angle(compMean) + phaseUnc), ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
f3.semilogx(tempFreq, 180.0/np.pi * (np.angle(compMean) - phaseUnc), ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)

f2.semilogx(tempFreq, np.zeros(freqLen), lw=3, color=tableau20[colorKey], label='Mean', alpha = alphaValue)
f2.semilogx(tempFreq, 100.0 *  magUnc/np.abs(compMean), ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha = alphaValue)
f2.semilogx(tempFreq, 100.0 * -magUnc/np.abs(compMean), ls='--', lw=3, color=tableau20[colorKey], alpha = alphaValue)

f4.semilogx(tempFreq, np.zeros(freqLen), lw=3, color=tableau20[colorKey], alpha = alphaValue)
f4.semilogx(tempFreq, 180.0/np.pi * phaseUnc, ls='--', lw=3, color=tableau20[colorKey], alpha = alphaValue)
f4.semilogx(tempFreq, -180.0/np.pi * phaseUnc, ls='--', lw=3, color=tableau20[colorKey], alpha = alphaValue)

# Plot settings and titles

if args.yAxisLimitPercent is not None:
  min2Range = -args.yAxisLimitPercent[0]
  max2Range = args.yAxisLimitPercent[0]
if args.yAxisLimitDegrees is not None:
  min4Range = -args.yAxisLimitDegrees[0]
  max4Range = args.yAxisLimitDegrees[0]
 
f1.set_xlim([minDomain, maxDomain])
f2.set_xlim([minDomain, maxDomain])
f3.set_xlim([minDomain, maxDomain])
f4.set_xlim([minDomain, maxDomain])

f1.set_ylim([min1Range, max1Range])
f2.set_ylim([min2Range, max2Range])
f3.set_ylim([min3Range, max3Range])
f4.set_ylim([min4Range, max4Range])

f3.set_yticks([-180, -90, 0, 90, 180])
f1.grid(which='minor')
f1.set_axisbelow(True)
f2.grid(which='minor')
f2.set_axisbelow(True)
f3.grid(which='minor')
f3.set_axisbelow(True)
f4.grid(which='minor')
f4.set_axisbelow(True)

f1.set_ylabel(yAxisLabel)
f2.set_ylabel(y2AxisLabel)
f3.set_ylabel(y3AxisLabel)
f4.set_ylabel(y4AxisLabel)

f3.set_xlabel(xAxisLabel)
f4.set_xlabel(xAxisLabel)
f1.legend(loc='best')
f2.legend(loc='best')

f1.set_title(plotLeftTitle)
f2.set_title(plotRightTitle)

h.suptitle(plotTitle +' - CTN Lab '+ curDateLabel)
plt.savefig(pathToPlots + plotSaveName +'_FourSquare.pdf',bbox_inches='tight',pad_inches=0.2)

# Make da plot
h = plt.figure(figsize=(16,12))
f1 = h.add_subplot(211)
f3 = h.add_subplot(212)
max1Range = -np.inf
min1Range = np.inf
max2Range = -np.inf
min2Range = np.inf
max3Range = -np.inf
min3Range = np.inf
max4Range = -np.inf
min4Range = np.inf

maxDomain = -np.inf
minDomain = np.inf
alphaValue = 0.1
colorKey = 4

for key in plotDict:
  tempFreq = plotDict[key][:,0]
  tempSpec = plotDict[key][:,1]
  tempPhas = np.pi/180.0 * plotDict[key][:,2]
  
  if loglog == True:
    f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)
  elif semilogx == True:
    f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)
  else:
    f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[colorKey], label=labels[key], alpha=alphaValue)

  f3.semilogx(tempFreq, 180.0/np.pi * tempPhas, lw=3, color=tableau20[colorKey], alpha=alphaValue)
  
  if max(tempSpec) > max1Range:
    max1Range = max(tempSpec)
  if min(tempSpec) < min1Range:
    min1Range = min(tempSpec)

  if max(180.0/np.pi*tempPhas) > max3Range:
    max3Range = max(180.0/np.pi*tempPhas)
  if min(180.0/np.pi*tempPhas) < min3Range:
    min3Range = min(180.0/np.pi*tempPhas)
 
  if max(tempFreq) > maxDomain:
    maxDomain = max(tempFreq)
  if min(tempFreq) < minDomain:
    minDomain = min(tempFreq)

colorKey = 0
alphaValue = 0.6
if loglog == True:
  f1.loglog(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.loglog(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.loglog(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
elif semilogx == True:
  f1.semilogx(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.semilogx(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.semilogx(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
else:
  f1.plot(tempFreq, np.abs(compMean), lw=3, color=tableau20[colorKey], label='Mean', alpha=alphaValue)
  f1.plot(tempFreq, np.abs(compMean) + magUnc, ls='--', lw=3, color=tableau20[colorKey], label='$\pm 1 \sigma$ Unc', alpha=alphaValue)
  f1.plot(tempFreq, np.abs(compMean) - magUnc, ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)

f3.semilogx(tempFreq, 180.0/np.pi * np.angle(compMean), lw=3, color=tableau20[colorKey], alpha=alphaValue)
f3.semilogx(tempFreq, 180.0/np.pi * (np.angle(compMean) + phaseUnc), ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)
f3.semilogx(tempFreq, 180.0/np.pi * (np.angle(compMean) - phaseUnc), ls='--', lw=3, color=tableau20[colorKey], alpha=alphaValue)

# Plot settings and titles

f1.set_xlim([minDomain, maxDomain])
f3.set_xlim([minDomain, maxDomain])

f1.set_ylim([min1Range, max1Range])
f3.set_ylim([min3Range, max3Range])

f3.set_yticks([-180, -90, 0, 90, 180])
f1.grid(which='minor')
f1.set_axisbelow(True)
f3.grid(which='minor')
f3.set_axisbelow(True)

f1.set_ylabel(yAxisLabel)
f3.set_ylabel(y3AxisLabel)

f3.set_xlabel(xAxisLabel)
f1.legend(loc='best')

f1.set_title(plotTitle + ' - CTN Lab ' + curDateLabel)

plt.savefig(pathToPlots + plotSaveName +'_JustBode.pdf',bbox_inches='tight',pad_inches=0.2)

# Merge PDFs into one page
command = 'gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -sOutputFile=' + pathToPlots + plotSaveName +'.pdf' + ' ' + pathToPlots + plotSaveName + '_FourSquare.pdf ' + pathToPlots + plotSaveName +'_JustBode.pdf'
print 
print 'Running $', command
print
os.system(command)

# Make tar automatically if --tar is set
if args.tar is True:
  tarCommand = 'tar -cvzf '+ pathToPlots + plotSaveName +'.tar.gz '
  for ff in args.files:
    tarCommand = tarCommand + ff.name + ' '
  tarCommand = tarCommand + pathToPlots + plotSaveName +'_Merged.pdf '
  tarCommand = tarCommand + __file__
  print
  print 'Running $', tarCommand
  print
  os.system(tarCommand)
