import numpy as np
import argparse
import time
import os

################################
##### SET THE BELOW PARAMS #####
################################
isCsv = False # Set if reading in .csv files

################################

# This is now in a seperate def
args = getInputArgs()



labels = np.array([])
for tempFile in np.append(vars(args)['spectrum'], vars(args)['OLG'] ):
  labels = np.append(labels, tempFile.name.split('/')[-1].replace('_', ' '))
  print tempFile.name
print args.LO

curDateLabel = time.strftime("%b %d %Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

# Check if save folder exists, and if it doesn't, make them
todaysDate = time.strftime("%Y%m%d")
saveFolderName = todaysDate + '_CalibratedBeatnoteSpectrum/'
dataSaveFolderName =  '../data/'+saveFolderName

if not os.path.isdir(plotsSaveFolderName):
  os.makedirs(plotsSaveFolderName)
if not os.path.isdir(dataSaveFolderName):
  os.makedirs(dataSaveFolderName)
print 'Plots made in  ', plotsSaveFolderName
print 'Data stored in ', dataSaveFolderName

plotDict = {}
for ii, arg in enumerate(np.append(args.spectrum, args.OLG)):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')


### Calibrate the spectrum using the OLG ###
LO = args.LO[0] * 1e3 / np.sqrt(2.0) # Put the Marconi settings from kHz/Vp to Hz/Vrms (Vrms/Vp = 1/np.sqrt(2), Hz/kHz = 1e3)
# todo: consider using SI units in all cases, i.e. Hz instead of kHz - awade

SpectrumFreq = plotDict[0][:,0]
SpectrumMag = plotDict[0][:,1] # Should be in Vrms/rtHz.  If not that's bad

index = np.argwhere(np.abs(SpectrumFreq) < 0.000001)
# todo: consider just cutting off an integer number of bins around DC - awade
SpectrumFreq = np.delete(SpectrumFreq, index)
SpectrumMag = np.delete(SpectrumMag, index)

OLGFreq = plotDict[1][:,0]
OLGMag = plotDict[1][:,1]
OLGPhase = plotDict[1][:,2]

### Find the first UGF of the PLL OLG ###
# Find the closest first index to the OLG UGF
UGFidx = np.where((OLGMag - 1.0) < 0.0)[0][0] # Find the first index where mag < 1.0
# Interpolate to find the actual UGF
y1 = OLGMag[UGFidx - 1]
y2 = OLGMag[UGFidx]
x1 = OLGFreq[UGFidx - 1]
x2 = OLGFreq[UGFidx]
m = (y2-y1)/(x2-x1)
b = y1 - m * x1
PLL_UGF = (1.0 - b)/m

# Create a function which takes in a frequency vector and UGF and outputs a PLL OLG
# This is basically an integrator, some UGF/f function
def PLLOLGfunc(freqVec, UGF):
    return UGF/freqVec

#OLGMagInterp = np.interp(SpectrumFreq, OLGFreq, OLGMag)
#OLGPhaseInterp = np.interp(SpectrumFreq, OLGFreq, OLGPhase)
OLG = PLLOLGfunc(SpectrumFreq, PLL_UGF) # ignore phase

PLL = OLG/(1.0 - OLG) # Get PLL loop suppression
# todo: this is the close loop TF, the supression function is 1/(1.0-OLG)
#       need to confirm if the read out is before or after the BN injection
#       injection point in the PLL loop.  -awade

CalibratedSpectrum = LO * SpectrumMag / np.abs(PLL) # Puts Vrms/rtHz into Hz/rtHz

# Save the data
saveTxt = np.vstack((SpectrumFreq, CalibratedSpectrum)).T
np.savetxt(dataSaveFolderName + curDate + '_' + plotSaveName + '.txt' , saveTxt, header='Frequency [Hz], Spectrum [Hz/rtHz]')

# Make the plot
h = plt.figure(figsize=(16,12))
ax = h.gca()
maxRange = -np.inf
minRange = np.inf
maxDomain = -np.inf
minDomain = np.inf

tempFreq = SpectrumFreq
tempSpec = CalibratedSpectrum

if loglog == True:
  plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
elif semilogx == True:
  plt.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
elif semilogy == True:
  plt.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
else:
  plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)

if max(tempSpec) > maxRange:
  maxRange = max(tempSpec)
if min(tempSpec) < minRange:
  minRange = min(tempSpec)
if max(tempFreq) > maxDomain:
  maxDomain = max(tempFreq)
if min(tempFreq) < minDomain:
  minDomain = min(tempFreq)
ax.grid(which='minor')
plt.xlim(minDomain, maxDomain)
plt.ylim(minRange, maxRange)
plt.xlabel(xAxisLabel)
plt.ylabel(yAxisLabel)
plt.legend(loc='best')
ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)

fullPlotName = '../plots/'+ saveFolderName + curDate +'_'+ plotSaveName +'.pdf'
plt.savefig(fullPlotName, bbox_inches='tight', pad_inches=0.2)
#plt.savefig('../plots/'+ saveFolderName + curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)

# For all plots, if --openPlot tag is on, open the plot
#if openPlot:
command = 'open ' + fullPlotName
print
print 'Opening Plot: '
print command
print
os.system(command)

def getInputArgs():
    parser = argparse.ArgumentParser(description='Usage: python calibratedBeatnoteSpectrum.py Vrms_per_rtHz_Spectrum.txt PLL_OLG.txt Marconi_FM_Mod_kHz_per_Vpeak [--xlabel "myXlabel"] [--ylabel "myYlabel"] [--y2label "myY2label"] [--title "myTitle"]')
    parser.add_argument('spectrum', type=argparse.FileType('r'), nargs=1)
    parser.add_argument('OLG', type=argparse.FileType('r'), nargs=1)
    parser.add_argument('LO', type=float, nargs=1)
    parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
    parser.add_argument('--ylabel', type=str, nargs=1, help='If defined, becomes the ylabel of the plot')
    parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the title of the plot')

    return args = parser.parse_args()
