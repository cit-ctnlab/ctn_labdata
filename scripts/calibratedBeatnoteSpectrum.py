import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34,
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 14,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
##### SET THE BELOW PARAMS #####
################################
loglog = True   # Set if we would like a loglog plot
semilogx = False # Set if we want semilogx plot
semilogy = False # Set if we want semilogy plot
# if none of the above are set, we do a regular linear plot
isCsv = False # Set if reading in .csv files

xAxisLabel = r'Frequency [Hz]'
yAxisLabel = r'ASD of Trans Beatnote [$Hz$/$\sqrt{Hz}$]'
plotTitle = r'Frequency Noise ASD'
plotSaveName = 'CalibratedSpectrum'
################################

parser = argparse.ArgumentParser(description='Usage: python calibratedBeatnoteSpectrum.py Vrms_per_rtHz_Spectrum.txt PLL_OLG.txt Marconi_FM_Mod_kHz_per_Vrms [--xlabel "myXlabel"] [--ylabel "myYlabel"] [--y2label "myY2label"] [--title "myTitle"]')
parser.add_argument('spectrum', type=argparse.FileType('r'), nargs=1,
                    help='PLL Error Signal Spectrum .txt file in units Vrms/rtHz')
parser.add_argument('OLG', type=argparse.FileType('r'), nargs=1,
                    help='PLL open loop gain TF measurement file.  Used to find the PLL unity gain frequency.')
parser.add_argument('LO', type=float, nargs=1,
                    help='Marconi Local Oscillator setting. FM Devn in kHz/Vrms.')
parser.add_argument('Preamp', type=float, nargs=1,
                    help='SR560 Preamp gain factor.')
parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
parser.add_argument('--ylabel', type=str, nargs=1, help='If defined, becomes the ylabel of the plot')
parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the title of the plot')

args = parser.parse_args()

if args.xlabel is not None:
  xAxisLabel = args.xlabel[0]
if args.ylabel is not None:
  yAxisLabel = args.ylabel[0]
if args.title is not None:
  plotTitle = args.title[0]
print 'xlabel =  ', xAxisLabel
print 'ylabel =  ', yAxisLabel
print 'title =   ', plotTitle

labels = np.array([])
for tempFile in np.append(vars(args)['spectrum'], vars(args)['OLG'] ):
  labels = np.append(labels, tempFile.name.split('/')[-1].replace('_', ' '))
  print tempFile.name
print args.LO

curDateLabel = time.strftime("%b %d %Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

# Check if save folder exists, and if it doesn't, make them
todaysDate = time.strftime("%Y%m%d")
saveFolderName = todaysDate + '_CalibratedBeatnoteSpectrum/'
plotsSaveFolderName = '../plots/'+saveFolderName
dataSaveFolderName =  '../data/'+saveFolderName

if not os.path.isdir(plotsSaveFolderName):
  os.makedirs(plotsSaveFolderName)
if not os.path.isdir(dataSaveFolderName):
  os.makedirs(dataSaveFolderName)
print 'Plots made in  ', plotsSaveFolderName
print 'Data stored in ', dataSaveFolderName

plotDict = {}
for ii, arg in enumerate(np.append(args.spectrum, args.OLG)):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

tableau20 = [(31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138),
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

### Calibrate the spectrum using the OLG ###
LO = args.LO[0] * 1e3  # Put the Marconi settings from kHz/Vrms to Hz/Vrms (Hz/kHz = 1e3)
PreampGain = args.Preamp[0] # SR560 Preamp gain factor
# todo: consider using SI units in all cases, i.e. Hz instead of kHz - awade

SpectrumFreq = plotDict[0][:,0]
SpectrumMag = plotDict[0][:,1] # Should be in Vrms/rtHz.  If not that's bad

index = np.argwhere(np.abs(SpectrumFreq) < 0.000001)
# todo: consider just cutting off an integer number of bins around DC - awade
SpectrumFreq = np.delete(SpectrumFreq, index)
SpectrumMag = np.delete(SpectrumMag, index)

OLGFreq = plotDict[1][:,0]
OLGMag = plotDict[1][:,1]
OLGPhase = plotDict[1][:,2]

### Find the first UGF of the PLL OLG ###
# Find the closest first index to the OLG UGF
UGFidx = np.where((OLGMag - 1.0) < 0.0)[0][0] # Find the first index where mag < 1.0
# Interpolate to find the actual UGF
y1 = OLGMag[UGFidx - 1]
y2 = OLGMag[UGFidx]
x1 = OLGFreq[UGFidx - 1]
x2 = OLGFreq[UGFidx]
m = (y2-y1)/(x2-x1)
b = y1 - m * x1
PLL_UGF = (1.0 - b)/m

# Create a function which takes in a frequency vector and UGF and outputs a PLL OLG
# This is basically an integrator, some UGF/f function
def PLLOLGfunc(freqVec, UGF):
    return UGF/freqVec

#OLGMagInterp = np.interp(SpectrumFreq, OLGFreq, OLGMag)
#OLGPhaseInterp = np.interp(SpectrumFreq, OLGFreq, OLGPhase)
OLG = PLLOLGfunc(SpectrumFreq, PLL_UGF) # ignore phase

PLL = OLG/(1.0 + OLG) # Get PLL loop suppression
# todo: this is the close loop TF, the supression function is 1/(1.0-OLG)
#       need to confirm if the read out is before or after the BN injection
#       injection point in the PLL loop.  -awade

CalibratedSpectrum = PreampGain * LO * SpectrumMag / np.abs(PLL) # Puts Vrms/rtHz into Hz/rtHz

# Save the data
saveTxt = np.vstack((SpectrumFreq, CalibratedSpectrum)).T
np.savetxt(dataSaveFolderName + curDate + '_' + plotSaveName + '.txt' , saveTxt, header='Frequency [Hz], Spectrum [Hz/rtHz]')
print 'Calibrated Data saved to', dataSaveFolderName + curDate + '_' + plotSaveName + '.txt'

# Make the plot
h = plt.figure(figsize=(16,12))
ax = h.gca()
maxRange = -np.inf
minRange = np.inf
maxDomain = -np.inf
minDomain = np.inf

tempFreq = SpectrumFreq
tempSpec = CalibratedSpectrum

if loglog == True:
  plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
elif semilogx == True:
  plt.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
elif semilogy == True:
  plt.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)
else:
  plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[0], label=labels[0], alpha=0.75)

if max(tempSpec) > maxRange:
  maxRange = max(tempSpec)
if min(tempSpec) < minRange:
  minRange = min(tempSpec)
if max(tempFreq) > maxDomain:
  maxDomain = max(tempFreq)
if min(tempFreq) < minDomain:
  minDomain = min(tempFreq)
ax.grid(which='minor')
plt.xlim(minDomain, maxDomain)
plt.ylim(minRange, maxRange)
plt.xlabel(xAxisLabel)
plt.ylabel(yAxisLabel)
plt.legend(loc='best')
ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)

fullPlotName = '../plots/'+ saveFolderName + curDate +'_'+ plotSaveName +'.pdf'
plt.savefig(fullPlotName, bbox_inches='tight', pad_inches=0.2)
#plt.savefig('../plots/'+ saveFolderName + curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)

# For all plots, if --openPlot tag is on, open the plot
#if openPlot:
command = 'open ' + fullPlotName
print
print 'Opening Plot: '
print command
print
os.system(command)
