import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34,
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
##### SET THE BELOW PARAMS #####
################################
isCsv = False # Set if reading in .csv filesi
################################

parser = argparse.ArgumentParser(description='Spectrum convertor script.  Math based on Heinzel, Table 1: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.455.7976&rep=rep1&type=pdf')
parser.add_argument('files', type=argparse.FileType('r'), nargs=1, help='Spectrum file we want to convert from one type of units to another.')
parser.add_argument('window', '-w', type=str, help='Window used to take the input spectrum.  This will be used to find the normalized equivalent noise bandwidth (NENBW)\n
                                                    These are taken from http://rfmw.em.keysight.com/wireless/helpfiles/89600B/WebHelp/Subsystems/gui/Content/windows_shapefactor_and_equiv_noisebw.htm \n
                                                    Window                        NENBW\n
                                                    -------------------------------------------\n
                                                    Uniform                       1.00 Hz-sec  \n
                                                    Hanning                       1.50 Hz-sec  \n
                                                    Blackman-Harris or BMH        2.0044 Hz-sec\n 
                                                    Kaiser-Bessel or KB           2.0013 Hz-sec\n
                                                    Gaussian                      2.0212 Hz-sec\n
                                                    Gaussian Top                  2.215 Hz-sec \n
                                                    Flat Top                      3.8194 Hz-sec\n')
parser.add_argument('inputUnits', '-i', type=str, help='Units of the input spectrum.  Valid inputs are PSD, PS, ASD, AS for Power Spectral Density, Power Spectrum, Amplitude Spectral Density, and Amplitude Spectrum.  Can also use LSD and LS for Linear Spectral Density and Linear Spectrum.')
parser.add_argument('outputUnits', '-o', type=str, help='Units of the output spectrum.  Same convention as above.')
args = parser.parse_args()

# Find normalized equivalent noise bandwidth from window used
if args.window == 'Uniform':
    NENBW = 1.0
elif args.window == 'Hanning':
    NENBW = 1.5
elif args.window == 'Blackman-Harris' or args.window == 'BMH':
    NENBW = 2.0044
elif args.window == 'Kaiser-Bessel' or args.window == 'KB':
    NENBW = 2.0013
elif args.window == 'Gaussian':
    NENBW = 2.0212
elif args.window == 'Gaussian Top':
    NENBW = 2.215
elif args.window == 'Flat Top':
    NENBW = 3.8194
else:
    print
    print 'Window not found, please choose Uniform, Hanning, BMH, KB, Gaussian, Gaussian Top, or Flat Top'
    print 'Exiting...'
    sys.exit()

# Find input units
if args.inputUnits == 'PSD':
    iUnits = args.inputUnits
elif args.inputUnits == 'PS':
    iUnits = args.inputUnits
elif args.inputUnits == 'ASD' or args.inputUnits == 'LSD':
    iUnits = 'ASD'
elif args.inputUnits == 'AS' or args.inputUnits == 'LS':
    iUnits = 'AS'
else:
    print
    print 'Unit type not found, please choose PSD, PS, ASD, or AS'
    print 'Exiting...'
    sys.exit()

# Find output units
if args.outputUnits == 'PSD':
    oUnits = args.outputUnits
elif args.outputUnits == 'PS':
    oUnits = args.outputUnits
elif args.outputUnits == 'ASD' or args.outputUnits == 'LSD':
    oUnits = 'ASD'
elif args.outputUnits == 'AS' or args.outputUnits == 'LS':
    oUnits = 'AS'
else:
    print
    print 'Unit type not found, please choose PSD, PS, ASD, or AS'
    print 'Exiting...'
    sys.exit()

#### Start script ####
curDate = time.strftime("%Y%m%d_%H%M%S")

# Alter the plot directory path and plot filename
pathToData = os.path.dirname(os.path.abspath(args.files[-1].name)) + '/'
stitchedFileName = curDate +'_'+ args.inputUnits +'_to_'+ args.outputUnits +'_Spectrum.txt'
print 'Data is in              ', pathToData
print 'Stitched Spectrum is in ', stitchedFileName 

# Read in data
plotDict = {}
for ii, arg in enumerate(args.files):
    if isCsv == False:
        plotDict[ii] = np.loadtxt(arg)
    else:
        plotDict[ii] = np.loadtxt(arg, delimiter=',')
    if ii == 1:
        print
        print 'Warning: Make sure all your input spectra are the same units!'
        print

resFreqs = np.array([]) # Resolution frequencies
for key in plotDict.keys():
    plotDict[key] # NOT DONE

if iUnits == 'PSD':
    if oUnits == 'PSD':
        print
        print 'No unit change necessary'
        print 'Exiting...'
    if oUnits == 'PS':
        multipier = 
    if oUnits == 'ASD':
    if oUnits == 'AS':
elif iUnits == 'PS':
    if oUnits == 'PSD':
    if oUnits == 'PS':
    if oUnits == 'ASD':
    if oUnits == 'AS':
elif iUnits == 'ASD':
    if oUnits == 'PSD':
    if oUnits == 'PS':
    if oUnits == 'ASD':
    if oUnits == 'AS':
elif iUnits == 'AS':
    if oUnits == 'PSD':
    if oUnits == 'PS':
    if oUnits == 'ASD':
    if oUnits == 'AS':

allFreqs = plotDict[]
# Find f = 0 Hz and delete these points they are weird
# awade: this lowest bin basically includes the DC level, for various reasons
#        we discard it because normalizing correctly is difficult and nuanced
#        , choice of windows can change it, for instance. It looks like you 
#        are removing very low freq points, best just to remove the lowest or 
#        a couple of the lowest for a general purpose script, rather than
#        setting a arbitrary frequency cut off.
# craig: this isn't an arbitrary cutoff, this is for numerical precision.
#        I can't just set a float == 0.  Gotta do some f < tiny number.

index = np.argwhere(np.abs(allFreqs) < 0.000001)
allFreqs = np.delete(allFreqs, index)
allSpecs = np.delete(allSpecs, index)

# Sort the frequencies indicies, and use them to sort both allFreqs and allSpecs
indicies = np.argsort(allFreqs)
allFreqs = allFreqs[indicies]
allSpecs = allSpecs[indicies]

# Save the txt
saveTxt = np.vstack((allFreqs, allSpecs)).T
np.savetxt(pathToData + stitchedFileName, saveTxt, header='Frequency [Hz], Spectrum [Vrms/rtHz]')

