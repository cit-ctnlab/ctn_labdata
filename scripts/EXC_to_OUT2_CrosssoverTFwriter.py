import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24,
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24,
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt
import argparse
import time
import os
import sys

################################
################################
isCsv = False # Set if reading in .csv files

################################

parser = argparse.ArgumentParser(description='Usage: python EXC_to_OUT2_Crossoverpwriter.py FastEXCtoOUT2TF.txt EXC_to_OUT2_loopOpenTF.txt')
parser.add_argument('LoopOpenTF', type=argparse.FileType('r'), nargs=1, help='Txt file of OUT2/EXC TF with FSS loop open')
parser.add_argument('FastEXCtoOUT2TF', type=argparse.FileType('r'), nargs='+', help='Txts of OUT2/EXC TF with loop closed')

args = parser.parse_args()

for tempFile in vars(args)['LoopOpenTF']:
  print tempFile.name
for tempFile in vars(args)['FastEXCtoOUT2TF']:
  print tempFile.name

curDate = time.strftime("%Y%m%d_%H%M%S")

# Check which path, North or South
pathName = ''
if 'North' in tempFile.name:
  pathName = 'North'
elif 'South' in tempFile.name:
  pathName = 'South'
else:
  pathName = ''

# Check if save folder exists, and if it doesn't, make them
todaysDate = time.strftime("%Y%m%d")
saveFolderName = todaysDate + '_' + pathName +'_CrossoverTFfromEXCtoOUT2/'

dataSaveFolderName =  '../data/'+saveFolderName

if not os.path.isdir(dataSaveFolderName):
  os.makedirs(dataSaveFolderName)
print 'Data stored in ', dataSaveFolderName

plotDict = {}
for ii, arg in enumerate(args.FastEXCtoOUT2TF):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

arg = args.LoopOpenTF[0]
if isCsv == False:
  LoopOpenTxt = np.loadtxt(arg)
else:
  LoopOpenTxt = np.loadtxt(arg, delimiter=',')

# Loop Open TF
LoopOpenFreq = LoopOpenTxt[:,0]
LoopOpenMag = LoopOpenTxt[:,1]
LoopOpenPhase = np.pi/180.0 * LoopOpenTxt[:,2]
LoopOpen = LoopOpenMag * np.exp(1j*LoopOpenPhase)

for ii in np.arange(len(plotDict)):
  FastEXCtoOUT2Freq = plotDict[ii][:,0]
  FastEXCtoOUT2Mag = plotDict[ii][:,1]
  FastEXCtoOUT2Phase = np.pi/180.0 * plotDict[ii][:,2] # Data is already in degrees, convert to radians
  FastEXCtoOUT2 = FastEXCtoOUT2Mag * np.exp(1j*FastEXCtoOUT2Phase)

  if len(LoopOpenFreq) != len(FastEXCtoOUT2Freq):
    print
    print 'The length of the', ii, 'FastEXCtoOUT2TF is', len(FastEXCtoOUT2Freq), ', and the length of the loop open TF is', len(LoopOpenFreq)
    print 'Idiot'
    sys.exit()
  
  FastEXCtoOUT2 = FastEXCtoOUT2 / LoopOpen

  Crossover = 1.0 - 1.0/FastEXCtoOUT2 # FastEXCtoOUT2 = 1/(1 - Crossover)

  # Save the data
  saveCrossoverTxt = np.vstack((FastEXCtoOUT2Freq, np.abs(Crossover), 180.0/np.pi*np.angle(Crossover))).T
  
  plotSaveName = 'Crossover_from_' + os.path.basename(args.FastEXCtoOUT2TF[ii].name)
  print ii, '  Saving ', plotSaveName
  np.savetxt(dataSaveFolderName + plotSaveName , saveCrossoverTxt, header='Frequency [Hz], Magnitude, Phase [degs]')

# Make plot
#h = plt.figure(figsize=(16,12))
#f1 = h.add_subplot(211)
#f2 = h.add_subplot(212)
#max1Range = -np.inf
#min1Range = np.inf
#max2Range = -np.inf
#min2Range = np.inf
#maxDomain = -np.inf
#minDomain = np.inf
#
#tempFreq = FastEXCtoOUT2Freq
#labels = np.array(['Crossovers'])
#for key in np.arange(1):
#  if key == 0:
#    tempSpec = np.abs(Crossover)
#    tempPhas = 180.0/np.pi * np.angle(Crossover)
#  
#  if loglog == True:
#    f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
#  elif semilogx == True:
#    f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
#  else:
#    f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
#  f2.semilogx(tempFreq, tempPhas, lw=3, color=tableau20[key], alpha=0.75)
#  if max(tempSpec) > max1Range:
#    max1Range = max(tempSpec)
#  if min(tempSpec) < min1Range:
#    min1Range = min(tempSpec)
#
#  if max(tempPhas) > max2Range:
#    max2Range = max(tempPhas)
#  if min(tempPhas) < min2Range:
#    min2Range = min(tempPhas)
#
#  if max(tempFreq) > maxDomain:
#    maxDomain = max(tempFreq)
#  if min(tempFreq) < minDomain:
#    minDomain = min(tempFreq)
#
#f1.set_xlim([minDomain, maxDomain])
#f2.set_xlim([minDomain, maxDomain])
#f1.set_ylim([min1Range, max1Range])
#f2.set_ylim([min2Range, max2Range])
#f2.set_yticks([-180, -90, 0, 90, 180])
#f1.grid(which='minor')
#f1.set_axisbelow(True)
#f2.grid(which='minor')
#f2.set_axisbelow(True)
#f1.set_ylabel(yAxisLabel)
#f2.set_ylabel(y2AxisLabel)
#f2.set_xlabel(xAxisLabel)
#f1.legend(loc='best')
#f1.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
##plt.savefig('./'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
#plt.savefig(plotsSaveFolderName + plotSaveName +'.pdf',bbox_inches='tight',pad_inches=0.2)
#
