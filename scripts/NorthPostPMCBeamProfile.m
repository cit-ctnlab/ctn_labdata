%% a la mode demonstration
% mode matching and beam propagation solutions in MATLAB.
%
% a la mode is a stimple library of gaussian ABCD matrix tools intended to 
% optimize and visualize optical systems.

addpath('~/Git/alm/') % Also gets GaussProFit.m by awade
addpath('~/Git/alm/gwmcmc/')
%% Post PMC measurements
l_PMCtoLNP = 10.0e-2; % length from PMC to Post-PMC Lens
l_LNPtoSM1 = 3.5e-2;
l_SM1toBP = 2.5e-2; % length from 1st steering mirror to 1st set of beam profiler measurements
z_beamProfilerFront = l_PMCtoLNP + l_LNPtoSM1 + l_SM1toBP;
zz = linspace(0.0, 13.0, 14) * 0.0254 + z_beamProfilerFront;
wx = [611.1, 589.2, 572.1, 555.5, 562.0, 564.4, 573.0, 589.3, 609.6, 631.0, 664.0, 697.1, 734.0, 779.6] * 1.0e-6/2.0; % divide by two for beam diameters
wy = [615.6, 589.9, 576.3, 559.7, 555.9, 560.4, 568.0, 582.0, 605.0, 634.1, 658.5, 697.1, 735.8, 776.8] * 1.0e-6/2.0;

[w0x, z0x] = GaussProFit(zz, wx);
[w0y, z0y] = GaussProFit(zz, wy);

%% Post EOM measurements
l_SM1toEOM = 16.0e-2; % length from 1st steering mirror to 14.75 MHz resonant EOM front
l_EOM = 5.55e-2; %+ 0.1e-2*(2.2321 - 1.0); % length of EOM plus extra length from index of refraction ~ 2.23 for 4cm LiNbO3/MgO EOM crystal
l_EOMtoSM2 = 13.6e-2; % length from EOM back to 2nd steering mirror
l_SM2toSM3 = 10.1e-2; % length from 2nd to 3rd steering mirror, 
l_SM3toBP2 = 4.5e-2 - 10.16e-2;% length from 3rd steering mirror to second beam profile measurement front, 4.5e-2, -10.16e-2 for extra length added since meas
z_beamProfilerFront2 = l_PMCtoLNP + l_LNPtoSM1 + l_SM1toEOM + l_EOM + l_EOMtoSM2 + l_SM2toSM3 + l_SM3toBP2;
zz2 = [0.0, 1.0, 2.0, 3.0, 6.0, 7.0] * 0.0254 + z_beamProfilerFront2;
wx2 = [823.1, 866.0, 920.2, 967.5, 1130.0, 1177.1] * 1.0e-6/2.0;
wy2 = [800.0, 844.3, 899.0, 945.5, 1098.0, 1161.0] * 1.0e-6/2.0;

%% Post 3nd Steering Mirror Measurements
l_SM3toSM4 = 15.0e-2; % length from 3rd to 4th steering mirror
l_SM4toLN1 = 22.5e-2; % length from 4th steering mirror to Lens 1 center
l_LN1toFI = 7.75e-2; % Lens 1 center to front of Faraday Isolator
l_FI = 12.08e-2 + 12.5e-3 * (1.95 - 1); % Faraday Isolator length plus additional length from FI crystal
l_FItoLN2 = 19.90e-2; % Faraday Isolator back to Lens 2 center
l_LN2toNCAV = (23.6 + 8.0 + 30.54)*1e-2; % Lens 2 center to NCav back of 1st mirror.
l_MR1thickness = 0.5e-2; % NCav 1st mirror thickness
l_NCAV = 3.68e-2; % Length of NCav

l_SM4toBP3 = 2.2e-2; % length from 4th steering mirror to 3rd beam profile measurements
z_beamProfilerFront3 = l_PMCtoLNP + l_LNPtoSM1 + l_SM1toEOM + l_EOM + l_EOMtoSM2 + l_SM2toSM3 + l_SM3toSM4 + l_SM4toBP3;
zz3 = [0.0, 1.0, 2.0, 3.0] * 0.0254 + z_beamProfilerFront3;
wx3 = [1285.0, 1336.0, 1375.0, 1444.0] * 1.0e-6/2.0;
wy3 = [1271.0, 1325.0, 1382.0, 1435.0] * 1.0e-6/2.0;

% Get sum of lengths from PMC waist to NCAV
lenArr = [l_PMCtoLNP, l_LNPtoSM1, l_SM1toEOM, l_EOM, l_EOMtoSM2, l_SM2toSM3, l_SM3toSM4, l_SM4toLN1,...
          l_LN1toFI, l_FI, l_FItoLN2, l_LN2toNCAV, l_MR1thickness, l_NCAV]; % Create array of all lengths in meters
lenSum = cumsum(lenArr); % Create cumulative sum of all lengths from front of beam profiler.

% Alamode
% Seed waist 
w0_PMC = 332.0e-6; % from LittlePMC.ipynb, get seed waist
z0_PMC = 0.0;
% PostPMCLens
z_LNP = lenSum(1);
f_LNP = 343.63e-3;
% Lens 1
z_LN1 = lenSum(8); % Length from PMC waist to Lens1
f_LN1 = 103.1*1e-3; % focal lengths of Lens1 according to the datasheets on the lenses
% Lens 2
z_LN2 = lenSum(11); % Length from PMC waist to Lens1
f_LN2 = 154.7*1e-3; % focal lengths of Lens2 according to the datasheets on the lenses
% NCAV Mirror 1
z_NCAVM1 = lenSum(13);
RoC_NCAVM1 = -0.5;
% NCAV Mirror 2
z_NCAVM2 = lenSum(14);
RoC_NCAVM2 = 0.5;

goox = beamPath;

goox.seedWaist(w0_PMC, z0_PMC); % Calculated beam waist, put at beam waist
goox.addComponent(component.lens(f_LNP, z_LNP, 'PostPMCLens'));
goox.addComponent(component.lens(f_LN1, z_LN1, 'Lens1'));
goox.addComponent(component.lens(f_LN2, z_LN2, 'Lens2'));
goox.addComponent(component.curvedMirror(RoC_NCAVM1, z_NCAVM1, 'Mirror1'));
goox.addComponent(component.curvedMirror(RoC_NCAVM2, z_NCAVM2, 'Mirror2'));

gooy = beamPath;

gooy.seedWaist(w0_PMC, z0_PMC); % Calculated beam waist, put at beam waist
gooy.addComponent(component.lens(f_LNP, z_LNP, 'PostPMCLens'));
gooy.addComponent(component.lens(f_LN1, z_LN1, 'Lens1'));
gooy.addComponent(component.lens(f_LN2, z_LN2, 'Lens2'));
gooy.addComponent(component.curvedMirror(RoC_NCAVM1, z_NCAVM1, 'Mirror1'));
gooy.addComponent(component.curvedMirror(RoC_NCAVM2, z_NCAVM2, 'Mirror2'));

markerColor = [34/255.0, 139/255.0, 3/255.0];
markerColor2 = [25/255.0, 25/255.0, 112/255.0];
markerColor3 = [178/255.0, 34/255.0, 34/255.0];
plotFolder = ['~/Git/cit_ctnlab/ctn_labdata/plots/',datestr(date,'yyyymmdd'),'_NorthPathModeMatchingSolution/'];
if ~exist(plotFolder,'dir')
    mkdir(plotFolder)
end

plotExtend = 0.2;
zplot = z0_PMC-plotExtend : .002 : max(lenSum)+plotExtend;
goox.components
figure(100);
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
goox.plotSummary(zplot);
hold on;
h1 = plot(zz, wx, 'o', 'MarkerEdgeColor',markerColor);
h2 = plot(zz2, wx2, 'o', 'MarkerEdgeColor',markerColor2);
h3 = plot(zz3, wx3, 'o', 'MarkerEdgeColor',markerColor3); hold off;
title({['Current North Path Beam Profile from PMC to NCAV - X axis - ', datestr(date,'mmm dd, yyyy')],...
       ['PreEOMLens f = ', num2str(f_LNP*1e3), ' mm, Lens1 f = ', num2str(f_LN1*1e3), ' mm, Lens2 f = ', num2str(f_LN2*1e3), ' mm']})
legend([h1, h2, h3],{'Beam Profile Meas 1', 'Beam Profile Meas 2', 'Beam Profile Meas 3'})
saveas(gcf,[plotFolder,'PredictedPostPMCBeamProfileXAxis.png'])

% gooy.components
% figure(200);
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
% gooy.plotSummary(zplot);
% hold on;
% plot(zz, wy, 'o', 'MarkerEdgeColor',markerColor);
% plot(zz2, wy2, 'o', 'MarkerEdgeColor',markerColor2);
% plot(zz3, wy3, 'o', 'MarkerEdgeColor',markerColor3); hold off;
% title({['Current North Path Beam Profile from PMC to NCAV - Y axis - ', datestr(date,'mmm dd, yyyy')],...
%        ['PreEOMLens f = ', num2str(f_LNP*1e3), ' mm, Lens1 f = ', num2str(f_LN1*1e3), ' mm, Lens2 f = ', num2str(f_LN2*1e3), ' mm']})
% saveas(gcf,[plotFolder,'PredictedPostPMCBeamProfileYAxis.png'])

%% mode matching optimization
cavityLength = l_NCAV;
cavityRoC = RoC_NCAVM2;

targetBeamWaistSize = sqrt( lambda/(2*pi)*sqrt(cavityLength*(2*cavityRoC - cavityLength)) );

goox.targetWaist(targetBeamWaistSize, lenSum(end) - cavityLength/2.0); 
gooy.targetWaist(targetBeamWaistSize, lenSum(end) - cavityLength/2.0); 

% we create a list of possible lens choices
focalLengthList = [f_LN1, f_LN2, 0.1719191335, 0.2290772413, f_LNP, 0.4009963748, 0.4581544826, 0.57269, 0.68745];
lensList = component.lens(focalLengthList);

% lens distance degree of freedom
LN1backDOF = 30.0e-2;
LN1forwardDOF = 7.0e-2;
LN2backDOF = 19.0e-2;
LN2forwardDOF = 3.0e-2;
tic;
[pathList, overlapList] = goox.chooseComponents(...
                'Lens1', lensList, [z_LN1 - LN1backDOF z_LN1 + LN1forwardDOF], ...
                'Lens2', lensList.duplicate, [z_LN2 - LN2backDOF z_LN2 + LN2forwardDOF]);%, ... 
                %'-vt', 0.25); % set the minimum initial overlap to 0.25, if a combination of components
                             % has an overlap less than this, it will be skipped without trying to optimize the lens positions
toc
%Cut bits
%                 'lens2',lensList.duplicate,[3.5 4],... %duplicate the list, this allows
%                 ...                                    %  the same component to be chosen more than once
%                'target',[16*25.43e-3-10.2e-3,16*25.43e-3-10.2e-3]... % we can also allow the target waist position to vary while optimizing the overlap
%% Try MCMC Hammer
%define problem:
MCMCVariableNames = {'Lens1', 'Lens2'};
MCMCMeanValues = [z_LN1, z_LN2];
MCMCLogLikeWeights = [99.0, 2.0];
goox.setMCMCVariables(MCMCVariableNames);

% mu = [z_LN1; z_LN2];
% C = [1.0 0.0; 0.0 1.0];
% iC=pinv(C);
% logPfuns={@(m)-0.5*sum((m-mu)'*iC*(m-mu))}
%%
%make a set of starting points for the entire ensemble of walkers
numberOfWalkers = 10;
meanMatrix = repmat(MCMCMeanValues', 1, numberOfWalkers); % duplicate the mean vector values
lengthStd = 0.05; % 5 cm std
minit = meanMatrix + lengthStd * randn(length(MCMCVariableNames), numberOfWalkers);

%Apply the MCMC hammer
tic;
totalMCProposals = 100000;
[models,logP] = gwmcmc(minit, {@(MCMCVariableValues) goox.logLikelihood(MCMCVariableValues)}, totalMCProposals);
burnin = 0.2;
models(:, :, 1:floor(size(models,3)*burnin)) = []; %remove 20% as burn-in
models=models(:,:)'; %reshape matrix to collapse the ensemble member dimension
figure(50);
scatter(models(:,1),models(:,2))
prctile(models,[5 50 95]) 

toc
%% Postprocessing
figure(60);
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
ecornerplot(models, 'names', {'Lens1 Position z_1 [m]', 'Lens2 Position z_2 [m]'}, 'grid', true, 'color', markerColor2, 'binnumber', 100)
suptitle({'North Path Mode Matching MCMC Optimization', ...
          ['Lens1 f = ', num2str(f_LN1*1e3), ' mm, Lens2 f = ', num2str(f_LN2*1e3), ' mm']})
saveas(gcf,[plotFolder,'MCMCAlamodeOptimizedModeMatchingSolutionXAxis.png'])
%% select the best solution and plot it
% from the list of all solutions, we may choose the one which has good
% modematching, but also has minimal sensitivity to component location.
pathList = pathList(overlapList >= 0.95);
sensitivityList = pathList.positionSensitivity;
[sensitivityList,sortIndex] = sort(sensitivityList);
pathList = pathList(sortIndex);
pathList.positionSensitivity % Added by awade
pathList.targetOverlap % Added by awade

bestPath = pathList(1);


% print the component list to the command window
disp(' ')
disp(' Optimized Path Component List:')
display(bestPath.components)

bestLens1FocalLength = bestPath.component('Lens1').parameters.focalLength;
bestLens2FocalLength = bestPath.component('Lens2').parameters.focalLength;

figure(300);
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
bestPath.plotSummary(zplot);
hold on;
h1 = plot(zz, wx, 'o', 'MarkerEdgeColor',markerColor);
h2 = plot(zz2, wx2, 'o', 'MarkerEdgeColor',markerColor2);
h3 = plot(zz3, wx3, 'o', 'MarkerEdgeColor',markerColor3); hold off;
title({'Optimized North Path Mode Matching - X axis',['Lens1 f = ', num2str(bestLens1FocalLength*1e3), ' mm, Lens2 f = ', num2str(bestLens2FocalLength*1e3), ' mm']})
legend([h1, h2, h3],{'Beam Profile Meas 1', 'Beam Profile Meas 2', 'Beam Profile Meas 3'})
saveas(gcf,[plotFolder,'AlamodeOptimizedModeMatchingSolutionXAxis.png'])
