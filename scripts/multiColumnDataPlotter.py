
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34,
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
##### SET THE BELOW PARAMS #####
################################
loglog = True   # Set if we would like a loglog plot
semilogx = False # Set if we want semilogx plot
semilogy = False # Set if we want semilogy plot
# if none of the above are set, we do a regular linear plot

isCsv = False # Set if reading in .csv filesi

# User can set the labels and titles here and not in the command line if they please
xAxisLabel = 'X Axis (Be sure to set --xlabel)'
yAxisLabel = 'Y Axis (Be sure to set --ylabel)'
plotTitle = 'Plot Title (Be sure to set --title)'
#plotSaveName = 'NorthLaserSlowControlVoltageVsTransPower'
################################

parser = argparse.ArgumentParser(description='Usage: python twoColumnDataPlotter.py [--xlabel "Plot Xlabel"] [--ylabel "Plot Ylabel"] [--title "Plot Title"] ./path/to/data/folderDescription/any.txt\nThis will pull two column any.txt data files and plot them nicely in a folder called ./path/to/plots/folderDescription/any.pdf.  Can also handle as many data files you want, will plot ')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+')
parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
parser.add_argument('--ylabel', type=str, nargs=1, help='If defined, becomes the ylabel of the plot')
parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the title of the plot')
parser.add_argument('--loglog', action='store_true', help='If set, plots loglog plot.  This is the default')
parser.add_argument('--semilogx', action='store_true', help='If set, plots semilogx plot.')
parser.add_argument('--semilogy', action='store_true', help='If set, plots semilogy plot.')
parser.add_argument('--linear', action='store_true', help='If set, plots linear plot. Why are you doing this. Take a moment and consider your linear plot, and your life.')
parser.add_argument('--indexStart', type=int, nargs=1, help='If set, begins plotting with this data index')
parser.add_argument('--indexEnd', type=int, nargs=1, help='If set, cuts off data past this index')
args = parser.parse_args()

# Set the xlabel, ylabel, and title to the user defined ones
if args.xlabel is not None:
  xAxisLabel = args.xlabel[0]
if args.ylabel is not None:
  yAxisLabel = args.ylabel[0]
if args.title is not None:
  plotTitle = args.title[0]
print
print 'xlabel = ', xAxisLabel
print 'ylabel = ', yAxisLabel
print 'title = ', plotTitle
print

# If the user has selected something other than a loglog plot
if args.semilogx is True:
  loglog = False
  semilogx = True
if args.semilogy is True:
  loglog = False
  semilogy = True
if args.linear is True:
  loglog = False

# Make nice plot legend labels from the data filename
labels = np.array([])
for tempFile in args.files:
    tempFile.readline()
    tempLine = tempFile.readline() # Get the second line of the lines
    tempSplit = tempLine.split(' ')
    for ii in np.arange(2, len(tempSplit)):
        tempLabel = tempSplit[ii].replace('_',' ')
        labels = np.append(labels, tempLabel)
print labels
# Get data and time
curDateLabel = time.strftime("%b %d %Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

# Alter the plot directory path and plot filename
pathToData = os.path.dirname(os.path.abspath(tempFile.name)) + '/'
plotSaveName = os.path.basename(tempFile.name).replace('.txt', '') # plotSaveName is same as the last .txt file

# Check if equivalent /plots/ directory exists where the /data/ folder is
pathToPlots = pathToData.replace('/data/', '/plots/')
print 'Data is in         ', pathToData
print 'Plot will be in    ', pathToPlots
print 'Plot will be named ', plotSaveName
print
if not os.path.exists(pathToPlots):
    os.makedirs(pathToPlots)

# Read in data
plotDict = {}
for ii, arg in enumerate(args.files):
    if isCsv == False:
        plotDict[ii] = np.loadtxt(arg)
    else:
        plotDict[ii] = np.loadtxt(arg, delimiter=',')

# If user has selected plot indicies...
dataLen = len(plotDict[0][:,0])
if args.indexStart is not None:
    indexStart = args.indexStart[0]
    print 'indexStart = ', indexStart  
else:
    indexStart = 0
if args.indexEnd is not None:
    indexEnd = args.indexEnd[0]
    print 'indexEnd   = ', indexEnd
else:
    indexEnd = dataLen
if indexStart > dataLen or indexEnd > dataLen:
    print 'WARNING: the number of data points is ', dataLen
    print '         You choose data indicies from ', indexStart, ' to ', indexEnd

# Nice colors
tableau20 = [
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)

# Plot data
h = plt.figure(figsize=(16,12))
ax = h.gca()
maxDomain = -np.inf
minDomain = np.inf
for key in plotDict:
    tempFreq = plotDict[key][indexStart:indexEnd, 0]
    numRows, numColumns = np.shape(plotDict[key])
    for ii in np.arange(numColumns-1)[::-1]:
        tempSpec = plotDict[key][indexStart:indexEnd, ii+1]
        
        maxRange = -np.inf
        minRange = np.inf        
        if max(tempSpec) > maxRange:
            maxRange = max(tempSpec)
        if min(tempSpec) < minRange:
            minRange = min(tempSpec)
        totalRange = maxRange - minRange
        tempSpec = (tempSpec - minRange) / totalRange # Put everything in the [0,1] range

        if loglog == True:
            plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[2*ii], alpha=0.75,
                        label=labels[ii]+'\nMin = '+str(round(minRange,3))+ ' Max = '+str(round(maxRange,3)))
        elif semilogx == True:
            plt.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[2*ii], alpha=0.75,
                        label=labels[ii]+'\nMin = '+str(round(minRange,3))+ ' Max = '+str(round(maxRange,3)))
        elif semilogy == True:
            plt.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[2*ii], alpha=0.75,
                        label=labels[ii]+'\nMin = '+str(round(minRange,3))+ ' Max = '+str(round(maxRange,3)))
        else:
            plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[2*ii], alpha=0.75,
                        label=labels[ii]+'\nMin = '+str(round(minRange,3))+ ' Max = '+str(round(maxRange,3)))
          
    if max(tempFreq) > maxDomain:
        maxDomain = max(tempFreq)
    if min(tempFreq) < minDomain:
        minDomain = min(tempFreq)
ax.grid(which='minor')
plt.xlim(minDomain, maxDomain)
plt.ylim(-0.05, 1.05)
plt.xlabel(xAxisLabel)
plt.ylabel(yAxisLabel)
plt.legend(loc='best', fontsize=14)
ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)

fullPlotName = pathToPlots + plotSaveName +'.pdf'
plt.savefig(fullPlotName, bbox_inches='tight', pad_inches=0.2)

command = 'open ' + fullPlotName
print
print 'Opening Plot: '
print command
print
os.system(command)
