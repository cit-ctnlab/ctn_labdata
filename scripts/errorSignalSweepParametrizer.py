
import os
import sys
import time
import numpy as np
import argparse
import scipy.constants as scc

parser = argparse.ArgumentParser(description='errorSignalSweepParatrizer.py takes in a .csv file from an oscilloscope of the PDH error signal from a frequency sweep of the PZT. The sweep should feature a carrier reflection and a single sideband reflection.  This script assumes the modulation frequency is 14.75 MHz.  The output should be the measured PDH slope of the carrier locking point, and the calibration of the error signal in MHz/Vrms.')
parser.add_argument('errorSignalFile', type=str, help='File containing the PDH error signal sweep.  Assumed to be an .csv')
parser.add_argument('-m', '--modulationFrequency', type=float, default=14.75, help='EOM Modulation Frequency for sideband creation')
parser.add_argument('-t', '--saveTxt', action='store_true', help='Saves a txt of the linear fits to the slopes of the carrier and sideband error signals for ease of plotting later.  Default is false.')

args = parser.parse_args()

errorFile = np.loadtxt(args.errorSignalFile, delimiter=',')
time = errorFile[:,0]
volts = errorFile[:,1]

# Find error signal peak and trough corresponding to the carrier resonance
maxVoltIndex = np.argmax(volts)
minVoltIndex = np.argmin(volts)
maxVolts = volts[maxVoltIndex]
minVolts = volts[minVoltIndex]
if maxVoltIndex - minVoltIndex > 0:
    slopeSign = 1.0
else:
    slopeSign = -1.0
print maxVoltIndex
print minVoltIndex
print maxVolts
print minVolts
peak2peakVolts = slopeSign * (maxVolts - minVolts)


# Predicted PDH slope value
L = 3.68e-2 # m
FSR = scc.c / (2.0 * L) # Hz
Finesse = 15000.0
cavPole = FSR / (2.0 * Finesse) # Hz
modelPDHSlope = cavPole / peak2peakVolts # times two because you need the full linewidth
print 
print 'Free Spectral Range: ', FSR*1e-9, 'GHz'
print 'Cavity Pole:         ', cavPole*1e-3, 'kHz'
print 'Peak to Peak Volts:  ', peak2peakVolts, 'V'
print 'Model PDH Slope:     ', modelPDHSlope*1e-3, 'kHz/V'
print

# Measured PDH slope using the 14.75 MHz sidebands
modFreq = args.modulationFrequency * 1.0e6

### Search for sideband peak
# Search for peak away from carrier peak
surroundingRegion = 1000
sortedVoltIndicies = np.argsort(volts)
for voltIndex in sortedVoltIndicies:
    if np.abs(voltIndex - minVoltIndex) > surroundingRegion: # If minimum point is not near carrier peak
        minVoltIndex2 = voltIndex
        break
for voltIndex in reversed(sortedVoltIndicies): # Start at max and work down this time
    if np.abs(voltIndex - maxVoltIndex) > surroundingRegion: # If maximum point is not near carrier peak
        maxVoltIndex2 = voltIndex
        break
maxVolts2 = volts[maxVoltIndex2]
minVolts2 = volts[minVoltIndex2]
print 'MaxVoltIndex2', maxVoltIndex2
print 'MinVoltIndex2', minVoltIndex2
print 'Max Volts 2', maxVolts2
print 'Min Volts 2', minVolts2

# Now we have our two peaks region.  Search in between for the zero crossings, and interpolate
# Search for the carrier peak, fit a line, and find the time where volts = 0
indexSpan = np.abs(maxVoltIndex - minVoltIndex) / 4

if maxVoltIndex < minVoltIndex:
    lowIndex = maxVoltIndex + indexSpan
    highIndex = minVoltIndex - indexSpan
else:
    lowIndex = minVoltIndex + indexSpan
    highIndex = maxVoltIndex - indexSpan

timeRange = time[lowIndex:highIndex]
voltRange = volts[lowIndex:highIndex]
lineFit = np.polyfit(timeRange, voltRange, 1) # lineFit[0] = slope in V/s, lineFit[1] = V , Vfit = lineFit[0] * timeFit + lineFit[1]
carrierZeroTime = -1.0*lineFit[1]/lineFit[0] # time at which volts = 0
measPDHSlope = 1 / lineFit[0] # in s/V, need to convert to Hz/V

if args.saveTxt:
    print
    print 'Saving linear fits...'
    saveVolts = lineFit[0] * timeRange + lineFit[1]
    saveFilePath = args.errorSignalFile.replace( args.errorSignalFile.split('/')[-1], '' )
    saveData = np.vstack((timeRange, saveVolts)).T 
    np.savetxt(saveFilePath+'SouthPDHErrorSignal_CarrierSlopeLinearFit.csv', saveData, delimiter=',')

# Do the same for the sideband peak
indexSpan = np.abs(maxVoltIndex2 - minVoltIndex2) / 10

if maxVoltIndex2 < minVoltIndex2:
    lowIndex = maxVoltIndex2 + indexSpan
    highIndex = minVoltIndex2 - indexSpan
else:
    lowIndex = minVoltIndex2 + indexSpan
    highIndex = maxVoltIndex2 - indexSpan

timeRange = time[lowIndex:highIndex]
voltRange = volts[lowIndex:highIndex]
lineFit = np.polyfit(timeRange, voltRange, 1)
sidebandZeroTime = -1.0*lineFit[1]/lineFit[0] # time at which volts = 0

if args.saveTxt:
    saveVolts = lineFit[0] * timeRange + lineFit[1]
    saveData = np.vstack((timeRange, saveVolts)).T 
    np.savetxt(saveFilePath+'SouthPDHErrorSignal_SidebandSlopeLinearFit.csv', saveData, delimiter=',')


print
print 'Carrier Crossover Time:', carrierZeroTime
print 'Sideband Crossover Time:', sidebandZeroTime
print

timeToFreqConversion = modFreq / np.abs(carrierZeroTime - sidebandZeroTime) # in Hz/s
measPDHSlope = measPDHSlope * timeToFreqConversion 
print 'Measured PDH Slope:', measPDHSlope*1e-3, 'kHz/V'
print 'Modeled PDH Slope: ', modelPDHSlope*1e-3, 'kHz/V'

