import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24,
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24,
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
################################
loglog = True # Set if we would like a loglog plot
semilogx = False
plotPhase = True # Set if there is another row with phase information

isCsv = False # Set if reading in .csv files

xAxisLabel = 'Frequency [Hz]'
yAxisLabel = 'TF Magnitude [Hz/V]'
if plotPhase == True:
  y2AxisLabel = 'Phase [degs]' # Set y2 axis label

plotTitle = 'PZT and EOM Actuation TFs'
plotSaveName = 'PZTandEOMActuationTFs'
################################

parser = argparse.ArgumentParser(description='Usage: python PZTandEOMActuationTFplotter.py OLGTF.txt CrossoverTF.txt [--xlabel "myXlabel"] [--ylabel "myYlabel"] [--y2label "myY2label"] [--title "myTitle"]')
parser.add_argument('OLGTF', type=argparse.FileType('r'), nargs=1)
parser.add_argument('CrossoverTF', type=argparse.FileType('r'), nargs=1)
parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
parser.add_argument('--ylabel', type=str, nargs=1, help='If defined, becomes the ylabel of the plot')
parser.add_argument('--y2label', type=str, nargs=1, help='If defined, becomes the ylabel of the 2nd plot')
parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the title of the plot')

args = parser.parse_args()

if args.xlabel is not None:
  xAxisLabel = args.xlabel[0]
if args.ylabel is not None:
  yAxisLabel = args.ylabel[0]
if args.y2label is not None:
  y2AxisLabel = args.y2label[0]
if args.title is not None:
  plotTitle = args.title[0]
print 'xlabel =  ', xAxisLabel
print 'ylabel =  ', yAxisLabel
print 'y2label = ', y2AxisLabel
print 'title =   ', plotTitle

for tempFile in np.append(vars(args)['OLGTF'], vars(args)['CrossoverTF'] ):
  print tempFile.name

# Check which path, North or South
pathName = ''
if 'North' in tempFile.name:
  pathName = 'North'
elif 'South' in tempFile.name:
  pathName = 'South'
else:
  pathName = 'unknown'
plotTitle = pathName + ' ' + plotTitle
plotSaveName = pathName + plotSaveName

curDateLabel = time.strftime("%b-%d-%Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

# Check if save folder exists, and if it doesn't, make them
todaysDate = time.strftime("%Y%m%d")
saveFolderName = todaysDate + '_PZTandEOMActuationTFs/'
plotsSaveFolderName = '../plots/'+saveFolderName
dataSaveFolderName =  '../data/'+saveFolderName

if not os.path.isdir(plotsSaveFolderName):
  os.makedirs(plotsSaveFolderName)
if not os.path.isdir(dataSaveFolderName):
  os.makedirs(dataSaveFolderName)
print 'Plots made in  ', plotsSaveFolderName
print 'Data stored in ', dataSaveFolderName

plotDict = {}
for ii, arg in enumerate(np.append(args.OLGTF, args.CrossoverTF)):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

tableau20 = [(44, 160, 44), 
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
 
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

# Calculate PZT and EOM full actuation paths
OLGFreq = plotDict[0][:,0]
OLGMag = plotDict[0][:,1]
OLGPhase = np.pi/180.0 * plotDict[0][:,2] # Data is already in degrees, convert to radians
#OLG = OLGMag * np.exp(1j*OLGPhase)

CrossoverFreq = plotDict[1][:,0]
CrossoverMag = plotDict[1][:,1]
CrossoverPhase = np.pi/180.0 * plotDict[1][:,2]
Crossover = CrossoverMag * np.exp(1j*CrossoverPhase)

# Interpolate the OLG
OLGMagInterp = np.interp(CrossoverFreq, OLGFreq, OLGMag)
OLGPhaseInterp = np.interp(CrossoverFreq, OLGFreq, OLGPhase)
OLG = OLGMagInterp * np.exp(1j * OLGPhaseInterp)

# Based on Andrew's math (dubious ;)
PZT = Crossover*(OLG - 1.0)/(Crossover - 1.0)
EOM = (Crossover - OLG)/(Crossover - 1.0)

# Save the data
savePZTTxt = np.vstack((np.abs(PZT), 180.0/np.pi*np.angle(PZT))).T
saveEOMTxt = np.vstack((np.abs(EOM), 180.0/np.pi*np.angle(EOM))).T
np.savetxt(dataSaveFolderName + curDate + '_' + plotSaveName + '.txt' , savePZTTxt, header='Frequency [Hz], Spectrum [Hz/V], Phase [degs]')
np.savetxt(dataSaveFolderName + curDate + '_' + plotSaveName + '.txt' , saveEOMTxt, header='Frequency [Hz], Spectrum [Hz/V], Phase [degs]')
# Make plot
h = plt.figure(figsize=(16,12))
f1 = h.add_subplot(211)
f2 = h.add_subplot(212)
max1Range = -np.inf
min1Range = np.inf
max2Range = -np.inf
min2Range = np.inf
maxDomain = -np.inf
minDomain = np.inf

tempFreq = CrossoverFreq
labels = np.array(['PZT', 'EOM'])
for key in np.arange(2):
  if key == 0:
    tempSpec = np.abs(PZT)
    tempPhas = 180.0/np.pi * np.angle(PZT)
  else:
    tempSpec = np.abs(EOM)
    tempPhas = 180.0/np.pi * np.angle(EOM)

  if loglog == True:
    f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  elif semilogx == True:
    f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  else:
    f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  f2.semilogx(tempFreq, tempPhas, lw=3, color=tableau20[key], alpha=0.75)
  if max(tempSpec) > max1Range:
    max1Range = max(tempSpec)
  if min(tempSpec) < min1Range:
    min1Range = min(tempSpec)

  if max(tempPhas) > max2Range:
    max2Range = max(tempPhas)
  if min(tempPhas) < min2Range:
    min2Range = min(tempPhas)

  if max(tempFreq) > maxDomain:
    maxDomain = max(tempFreq)
  if min(tempFreq) < minDomain:
    minDomain = min(tempFreq)

f1.set_xlim([minDomain, maxDomain])
f2.set_xlim([minDomain, maxDomain])
f1.set_ylim([min1Range, max1Range])
f2.set_ylim([min2Range, max2Range])
f2.set_yticks([-180, -90, 0, 90, 180])
f1.grid(which='minor')
f1.set_axisbelow(True)
f2.grid(which='minor')
f2.set_axisbelow(True)
f1.set_ylabel(yAxisLabel)
f2.set_ylabel(y2AxisLabel)
f2.set_xlabel(xAxisLabel)
f1.legend(loc='best')
f1.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
#plt.savefig('./'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
plt.savefig(plotsSaveFolderName + curDate +'_'+ plotSaveName +'.pdf',bbox_inches='tight',pad_inches=0.2)

