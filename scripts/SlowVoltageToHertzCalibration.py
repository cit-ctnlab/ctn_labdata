# Craig Cahillane - October 11 2017
# SlowVoltageToHertzCalibration.py

import numpy as np
import matplotlib as mpl 
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24, 
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24, 
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt 
import argparse
import time
import os

parser = argparse.ArgumentParser(description='Usage: python SlowVoltageToHertzCalibration.py ./path/to/data/cavitySweep.txt.  Takes the cavity sweep, finds the highest resonances and their associated slow voltages, and calibrates slow voltage into hertz using the cavity FSR.')
parser.add_argument('cavitySweep', type=argparse.FileType('r'), nargs=1)
args = parser.parse_args()

tempTxt = np.loadtxt(args.cavitySweep[0])
slowVoltage = tempTxt[:,0]
transVoltage = tempTxt[:,1]

cc = 299792458.0 # speed of light
LL = 3.683e-2 # length of cavity
FSR = cc / (2.0 * LL) # Free Spectral range in Hz
print '*************************************'
print
print 'FSR =', FSR/10**6, 'MHz'
print
# Hard code some numbers to look for peaks in the cavity scan
maxTrans = 0.1 # This number defines how high a trans voltage peak must be
indicies = np.argwhere(transVoltage > maxTrans)[:,0]
indicies2 = np.array([], dtype=int) # make a deep copy of indicies for later reference
for index in indicies:
  for index2 in indicies:
    if index == index2: # once we get to the same index
      break # restart the index count
    if np.abs(slowVoltage[index] - slowVoltage[index2]) < 0.5: #If peaks are close
      if transVoltage[index] > transVoltage[index2]:
        indicies2 = np.append(indicies2, index2)
      else: 
        indicies2 = np.append(indicies2, index)

# Now indicies2 has our indicies of our FSR peaks.  Find the associated slowVoltages
# and average them
#print 'Peak indicies', indicies
#print '2nd order peaks indicies', indicies2

indicies3 = np.setdiff1d(indicies, indicies2)
print 'Transmission Peak Indicies', indicies3
slowFSRpeaks = slowVoltage[indicies3]
print 'Slow Voltage of Transmission Peaks ', slowFSRpeaks
print 'Number of peaks:', len(slowFSRpeaks)

peakDiffs = np.zeros(len(slowFSRpeaks)-1)
for ii in np.arange(len(slowFSRpeaks)-1):
  peak1 = slowFSRpeaks[ii]
  peak2 = slowFSRpeaks[ii + 1]
  peakDiffs[ii] = peak2 - peak1
peakDiffMean = np.mean(peakDiffs)

hertzPerSlowVolt = FSR/peakDiffMean
print '***********************************'
print
print 'Calibration:', hertzPerSlowVolt/10**6, ' MHz / SlowVolt'
print
print '***********************************'
