
import numpy as np
import matplotlib as mpl 
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12, 
                     'font.size': 34, 
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24, 
                     'axes.labelsize': 24, 
                     'xtick.color':'k',
                     'xtick.labelsize': 24, 
                     'ytick.color':'k',
                     'ytick.labelsize': 24, 
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24, 
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt 
import argparse
import time
from datetime import datetime
from datetime import timedelta
import os

################################
##### SET THE BELOW PARAMS #####
################################
isCsv = False # Set if reading in .csv filesi
################################

parser = argparse.ArgumentParser(description='Usage: python spectraStitcher.py spectrum_*.txt.  Produced a two column text file of the beatnote.')
parser.add_argument('files', type=str, nargs='+', help='Reads in many two column txts assuming the first column is frequency and the second is Vrms/rtHz.')
parser.add_argument('--minBeatnoteStrength', '-mBS', type=float, nargs=1, default=0.0, help='Minimum Beatnote Strength for a spectrum to count towards the total, otherwise noise might drown out the beatnote.  Default is 0.0.') 
args = parser.parse_args()

curDate = time.strftime("%Y%m%d_%H%M%S")

# Alter the plot directory path and plot filename
pathToData = os.path.dirname(os.path.abspath(args.files[-1])) + '/'
beatnoteFileName = curDate + '_TrackedBeatnote.txt'
print 'Data is in              ', pathToData
print 'Beatnote frequencies in ', beatnoteFileName

def key2datetime(dateStr):  
    '''Read in string with format yyyy-mm-dd-HH-MM-SS, where mm=month and MM=minute, return a datetime object'''
    return datetime.strptime(dateStr, "%Y-%m-%d-%H-%M-%S")

# Read in data
plotDict = {}
minBeatnoteStrength = args.minBeatnoteStrength[0]
for ii, arg in enumerate(args.files):
    with open(arg, 'r') as tempFile:
        # Split each filename to get the date and time
        fileName = arg.split('/')[-1]
        fileName = fileName.replace('-', '_')
        timeList = fileName.split('_')
        
        day    = timeList[2]
        month  = timeList[3]
        year   = timeList[4]
        hour   = timeList[5][0:2]
        minute = timeList[5][2:4]
        second = timeList[5][4:6]

        timeKey = year+'-'+month+'-'+day+'-'+hour+'-'+minute+'-'+second
        
        # Read in spectrum
        tempTxt = np.loadtxt(tempFile)
        tempFreq = tempTxt[:,0]
        tempSpec = tempTxt[:,1]
        
        # Find spectrum maximum using argmax
        maxIdx = tempSpec.argmax()
        maxFreq = tempFreq[maxIdx]
        maxSpec = tempSpec[maxIdx]
        if maxSpec > minBeatnoteStrength: # If a peak is tall enough to be considered a true beatnote
            plotDict[timeKey] = maxFreq

sortedKeys = sorted(plotDict.keys(), key=lambda x: key2datetime(x))
sortedDict = [ plotDict[sortedKey] for sortedKey in sortedKeys]
firstDate = key2datetime(sortedKeys[0]) # Get first time
totalSeconds = np.array([])
for key in sortedKeys:
    keyDate = key2datetime(key)
    tempTimeDelta = keyDate - firstDate # subtract two datetime objects to get a delta time object
    tempSeconds = tempTimeDelta.total_seconds()
    totalSeconds = np.append(totalSeconds, tempSeconds)
# Save the txt
saveTxt = np.vstack((totalSeconds, sortedDict)).T
np.savetxt(pathToData + beatnoteFileName, saveTxt, 
            header='Start Date and time (yyyy-mm-dd-HH-MM-SS): ' + sortedKeys[0] + '\n'
                   +'Delta Time [s], Beatnote Frequency [Hz]')

