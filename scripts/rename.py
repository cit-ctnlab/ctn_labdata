'''
Craig Cahillane - Oct 25, 2017
Run this script from whatever directory you want to rename a bunch of files in
'''
import numpy as np
import os

files = os.listdir('.')
for ff in files:
  if 'North' in ff:
    newName = 'South' + ff[5:]
    command = 'mv ' + ff + ' ' + newName
    os.system(command)
    print command


