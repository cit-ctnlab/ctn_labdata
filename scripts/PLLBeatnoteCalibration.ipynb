{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "# Coatings Thermal Noise PLL Calibration Script\n",
    "This script is used to calibrate our phased-locked loop error and control signals into beatnote frequency noise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "<img src=\"../plots/20171211_PLLOLG/PLLBlockDiagram.jpg\",width=900, height=300>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "### Some Math\n",
    "We are trying to measure coatings brownian noise.  This noise is imprinted on the optical beatnote frequency $f_{BN}$ in Hz.\n",
    "\n",
    "We typically measure the PLL control signal $c = H \\, e $ where $H$ is our SR560 preamp gain.  The control signal is input into a Marconi VCO $A$, which outputs a locking sine wave at frequency $f_A$ in Hz.  We lock $f_A$ and $f_{BN}$ together using a mixer $M$ and low pass filter $L$. The mixer has implicit integration $1/s$ since the mixer takes in radians and outputs volts:\n",
    "\n",
    "$$ e = L M \\left( \\phi_{BN} - \\phi_A \\right) = L M \\left( \\dfrac{f_{BN}}{i f} - \\dfrac{f_A}{i f} \\right)$$\n",
    "where $\\phi_{BN} = \\dfrac{f_{BN}}{i f}$ due to the mixer integration.  We also have \n",
    "$$ f_A = A H \\, e$$\n",
    "Putting this together gives\n",
    "$$ f_A = \\dfrac{A H L M}{i f} \\left( f_{BN} - f_A \\right) $$\n",
    "Let our open loop gain $G = \\dfrac{A H L M}{i f} = \\dfrac{f_{PLL}}{i f}$:\n",
    "$$ f_A = \\dfrac{G}{1 + G} \\, f_{BN} = \\dfrac{1}{1 + i \\frac{f}{f_{PLL}}} \\, f_{BN}$$\n",
    "\n",
    "We have a PLL pole $f_{PLL} = A H L M$ which depends strongly on our preamp gain $H$ and Marconi frequency modulation $A$.  $L M$ can also change, it depends on the strength of the beatnote.  We have measured the mixer and low pass filter transfer function <a href=\"https://nodus.ligo.caltech.edu:8081/PSL_Lab/1998\">here</a>, and found for beatnote strength of +2 dBm, $L M = 0.208 \\, \\text{V}/\\text{rad}$.\n",
    "\n",
    "Going back to our error signal $e = \\dfrac{f_A}{A H}$ and control signal $c = \\dfrac{f_A}{A}$, we can actually measure these signals with the SR785, which makes their closed loop transfer functions important:\n",
    "$$ e = \\dfrac{1\\,/\\,A H}{1 + i \\frac{f}{f_{PLL}}} \\, f_{BN}$$\n",
    "\n",
    "$$ c = \\dfrac{1\\,/\\,A}{1 + i \\frac{f}{f_{PLL}}} \\, f_{BN}$$\n",
    "\n",
    "To transfer error signal ASD $S_e(f)$ and control signal ASD $S_c(f)$ back to a beatnote ASD $S_{f_{BN}}(f)$, we use:\n",
    "$$ S_{f_{BN}}(f) = \\left|1 + i \\frac{f}{f_{PLL}}\\right| \\, A H \\, S_e(f)$$\n",
    "\n",
    "$$ S_{f_{BN}}(f) = \\left|1 + i \\frac{f}{f_{PLL}}\\right| \\, A \\, S_c(f)$$\n",
    "\n",
    "The error signal spectrum $S_e$ is the best estimator of the beatnote spectrum $ S_{f_{BN}}$ after the OLG unity gain frequency.\n",
    "The control signal spectrum $S_c$ is best before the UGF."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "# To the User of this Script\n",
    "1) Load your measurement ASD .txt file.  Make sure to indicate if it's in dB/rtHz, the default is unitless mag /rtHz\n",
    "\n",
    "2) Indicate it's type as 'error' or 'control'\n",
    "\n",
    "3) EITHER load your measured PLL OLG TF .txt, or type in your PLL OLG unity gain frequency manually.\n",
    "\n",
    "4) Type your Marconi VCO FM Devn $A$\n",
    "\n",
    "5) IF NECESSARY, type your preamp gain $H$\n",
    "\n",
    "6) Select if you want to save your figure.  If yes, the script will create/save the figure in the ../plots/ equivalent directory where your ../data/ file came from.\n",
    "\n",
    "Run the script.  It should produce some plots for you, and save them if you selected that"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "##############################\n",
    "##### Set user settings: #####\n",
    "##############################\n",
    "measFilePath = '~/Git/cit_ctnlab/ctn_labdata/data/20171213_PLLControlSignalSpectra/' # path/to/meas.txt\n",
    "measFileName = 'StitchedSpectrum_PLLControlSignalSpectrum_MarconiFMDevn_1kHz_PreampGain_20_Avg_20_Span_102p4kHz_13-12-2017_123343.txt'\n",
    "measFileType = 'control' # either 'error' or 'control'\n",
    "measUnits = 'mag' # either 'mag' or 'dB'\n",
    "measScaler = 0.5 # factor included to compensate S_meas(f) for impedance matching errors.  \n",
    "# Typically 0.5 for the control signal since SR785 has 1 MΩ input impedance and the SR560 has 50Ω and 600Ω output impedance.\n",
    "\n",
    "PLLOLGFilePath = '~/Git/cit_ctnlab/ctn_labdata/data/20171213_PLLOLG/' # path/to/PLLOLG.txt\n",
    "PLLOLGFileName = 'PLL_OLGTF_MarconiFMDevn_1kHz_PreampGain_20_13-12-2017_131423.txt' # either None or 'somePLLOLGTF.txt'\n",
    "PLLOLGUnits = 'mag' # either 'mag' or 'dB'\n",
    "PLLOLGScaler = 1.0\n",
    "\n",
    "PLLOLGUGF = 3.0e3 # Hz. AKA f_PLL from above.  MUST specify if PLLOLGFileName = None, otherwise this is not used.\n",
    "\n",
    "MarconiFMDevn = 1.0e3 # Hz/V\n",
    "\n",
    "PreampGain = 20 # SR560 gain V/V, won't be used on control signals\n",
    "\n",
    "saveFigs = True # Save figures produced of calibrated beatnote spectrum\n",
    "saveTxt = True # Save .txt of calibrated beatnote spectrum\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "import time\n",
    "import numpy as np\n",
    "if 'matplotlib.pyplot' not in sys.modules:\n",
    "    import matplotlib as mpl\n",
    "    mpl.use('Agg')\n",
    "    import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def createDir(path):\n",
    "    path = os.path.expanduser(path)\n",
    "    if not os.path.isdir(path):\n",
    "        os.makedirs(path)\n",
    "    return path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# expand our ~ in path names:\n",
    "measFilePath = os.path.expanduser(measFilePath)\n",
    "PLLOLGFilePath = os.path.expanduser(PLLOLGFilePath)\n",
    "\n",
    "# Dope plotting settings\n",
    "mpl.rcParams.update({'text.usetex': True,\n",
    "                     'text.color':'w',\n",
    "                     'lines.linewidth': 4,\n",
    "                     'lines.markersize': 12,\n",
    "                     'font.size': 34, \n",
    "                     'font.family': 'FreeSerif',\n",
    "                     'axes.grid': True,\n",
    "                     'axes.facecolor' :'w',\n",
    "                     'axes.labelcolor':'w',\n",
    "                     'axes.titlesize': 24,\n",
    "                     'axes.labelsize': 24,\n",
    "                     'xtick.color':'w',\n",
    "                     'xtick.labelsize': 24,\n",
    "                     'ytick.color':'w',\n",
    "                     'ytick.labelsize': 24,\n",
    "                     'grid.color': '#555555',\n",
    "                     'legend.facecolor':'k',\n",
    "                     'legend.fontsize': 18,\n",
    "                     'legend.borderpad': 0.6,\n",
    "                     'figure.figsize': (16, 12),\n",
    "                     'figure.facecolor' : 'k'})\n",
    "curDateLabel = time.strftime(\"%b %d %Y %H:%M:%S\")\n",
    "curDateAndTime = time.strftime(\"%Y%m%d_%H%M%S\")\n",
    "curDate = time.strftime(\"%Y%m%d\")\n",
    "\n",
    "# Find/create place to save calibrated data\n",
    "if saveTxt:\n",
    "    calibratedDataPath = '~/Git/cit_ctnlab/ctn_labdata/data/' + curDate +'_CalibratedBeatnoteSpectrum/'\n",
    "    calibratedDataPath = createDir(calibratedDataPath)\n",
    "    calibratedDataFileName = curDateAndTime +'_CalibratedBeatnoteSpectrum_from'+measFileType+'Signal.txt'\n",
    "\n",
    "# Find/create place to save plots\n",
    "if saveFigs:\n",
    "    plotPath = '~/Git/cit_ctnlab/ctn_labdata/plots/' + curDate +'_CalibratedBeatnoteSpectrum/'\n",
    "    plotPath = createDir(plotPath)\n",
    "    plotName = curDateAndTime+'_CalibratedBeatnoteSpectrum.pdf'\n",
    "    \n",
    "    if PLLOLGFileName is not None:\n",
    "        PLLplotPath = PLLOLGFilePath.replace('/data/','/plots/')\n",
    "        PLLplotPath = createDir(PLLplotPath)\n",
    "        PLLplotName = curDateAndTime+'_PLLOLGUGF.pdf'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# Load measurement txt\n",
    "meas = np.loadtxt(measFilePath + measFileName)\n",
    "freq = meas[:,0]\n",
    "if measUnits == 'mag':\n",
    "    Sf = meas[:,1] * measScaler\n",
    "elif measUnits == 'dB':\n",
    "    Sf = 20.0 * np.log10(meas[:,1]) * measScaler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "if measFileType == 'error':\n",
    "    Sf_cal = MarconiFMDevn * PreampGain * Sf\n",
    "elif measFileType == 'control':\n",
    "    Sf_cal = MarconiFMDevn * Sf\n",
    "else:\n",
    "    print 'ERROR'\n",
    "    print 'Please specify measFileType to \"error\" or \"control\"'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def PLLOLGfunc(freqVec, UGF):\n",
    "    return UGF/(1.0j*freqVec)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# If we uploaded a PLL OLG TF, find the unity gain frequency by fitting a line to the OLG TF data.\n",
    "if PLLOLGFileName is not None:\n",
    "    PLLOLGdata = np.loadtxt(PLLOLGFilePath + PLLOLGFileName)\n",
    "    PLLOLGfreq = PLLOLGdata[:,0]\n",
    "    if measUnits == 'mag':\n",
    "        PLLOLGmag = PLLOLGdata[:,1] * PLLOLGScaler\n",
    "    elif measUnits == 'dB':\n",
    "        PLLOLGmag = 20.0 * np.log10(PLLOLGdata[:,1]) * PLLOLGScaler\n",
    "    PLLOLGphase = PLLOLGdata[:,2]\n",
    "    \n",
    "    ### Find the first UGF of the PLL OLG ###\n",
    "    # Find the closest first index to the OLG UGF\n",
    "    UGFidx = np.where((PLLOLGmag - 1.0) > 0.0)[0][-1] # Find the last index where mag > 1.0\n",
    "    UGFidxMin = UGFidx - 10\n",
    "    \n",
    "    PLLOLGlogFreq = np.log10(PLLOLGfreq[UGFidxMin:])\n",
    "    PLLOLGlogMag = np.log10(PLLOLGmag[UGFidxMin:])\n",
    "    fit_coeffs = np.polyfit( PLLOLGlogFreq, PLLOLGlogMag, 1) # Fit a line\n",
    "    slope = fit_coeffs[0]\n",
    "    y0 = fit_coeffs[1]\n",
    "    \n",
    "    PLLOLGUGF = 10.0**((0.0 - y0) / slope) # Hz\n",
    "    PLLOLG = PLLOLGfunc(PLLOLGfreq, PLLOLGUGF)\n",
    "    \n",
    "    print 'Found PLL OLG unity gain frequency at', PLLOLGUGF, 'Hz'\n",
    "    \n",
    "    hh = plt.figure()\n",
    "    plt.loglog(PLLOLGfreq, PLLOLGmag, label='PLL OLG Meas')\n",
    "    plt.loglog(PLLOLGfreq, np.abs(PLLOLG), label='Linear Fit')\n",
    "    plt.axvline(x=PLLOLGUGF, ls='--', color='#B22222', label='Fit UGF = '+str(np.round(PLLOLGUGF))+' Hz')\n",
    "    plt.title('PLL OLG Measurement and Linear Fit \\n Marconi FM Devn = '+str(np.round(MarconiFMDevn))\\\n",
    "              +' Hz/V - Preamp Gain = '+str((np.round(PreampGain)))+ ' V/V')\n",
    "    plt.xlabel('Frequency [Hz]')\n",
    "    plt.ylabel('PLL OLG Mag')\n",
    "    plt.legend(loc='best')\n",
    "    plt.grid(which='both', linestyle='--', color='grey', alpha=0.5)\n",
    "    if saveFigs:\n",
    "        plt.savefig(PLLplotPath + PLLplotName, facecolor=hh.get_facecolor(), bbox_inches='tight')\n",
    "    plt.show()\n",
    "    \n",
    "PLLOLG = PLLOLGfunc(freq, PLLOLGUGF) # fit to the spectrum frequency vector we end up with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "calibratedSpectrum = np.abs(1.0 + 1.0/PLLOLG) * Sf_cal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# Save the data\n",
    "if saveTxt:\n",
    "    saveTxtData = np.vstack((freq, calibratedSpectrum)).T\n",
    "    np.savetxt(calibratedDataPath + calibratedDataFileName, saveTxtData, header='Frequency [Hz], Spectrum [Hz/rtHz]')\n",
    "    print 'Calibrated Data saved to', calibratedDataPath + calibratedDataFileName"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# Plot the calibrated spectrum\n",
    "print \n",
    "print 'PLL OLG UGF = ', PLLOLGUGF, 'Hz'\n",
    "print measFileType, 'signal spectrum used to calibrate'\n",
    "\n",
    "hh = plt.figure()\n",
    "tempLabel = measFileName.replace('_',' ')\n",
    "figureLabel = tempLabel[:len(tempLabel)/2] + '\\n' + tempLabel[len(tempLabel)/2:]\n",
    "\n",
    "plt.loglog(freq, calibratedSpectrum, label=figureLabel)\n",
    "plt.axvline(x=PLLOLGUGF, ls='--', color='#B22222', label='Fit UGF = '+str(np.round(PLLOLGUGF))+' Hz')\n",
    "plt.title('Calibrated Beatnote Spectrum - from ' + measFileType + ' signal - ' + curDateLabel)\n",
    "plt.xlabel('Frequency [Hz]')\n",
    "plt.ylabel('Beatnote Frequency ASD $\\mathrm{Hz}/\\sqrt{\\mathrm{Hz}}$')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(which='both', linestyle='--', color='grey', alpha=0.5)\n",
    "\n",
    "if saveFigs:\n",
    "    plt.savefig(plotPath + plotName, facecolor=hh.get_facecolor())\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
