import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24,
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24,
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt
import argparse
import time
import os

################################
################################
loglog = True # Set if we would like a loglog plot
semilogx = False
semilogy = False
plotPhase = True # Set if there is another row with phase information

isCsv = False # Set if reading in .csv files
isdB = False
openPlot = True

xAxisLabel = 'Frequency [Hz]'
yAxisLabel = 'TF Magnitude'
if plotPhase == True:
  y2AxisLabel = 'Phase [degs]' # Set y2 axis label

plotTitle = 'TF Measurement'
#plotSaveName = 'PLL_OLGTFs'
################################

parser = argparse.ArgumentParser(description='Usage: python TFplotter.py ./path/to/data/folderDescription/TF.txt   This takes three columns of data, frequency, magnitude and phase, and plots them well.  Puts plots in ./path/to/plots/folderDescription/TF.txt. Can handle many TFs, will put in the folder of the last data file included')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+')
parser.add_argument('--xlabel', type=str, nargs=1, help='If defined, becomes the xlabel of the plot')
parser.add_argument('--ylabel', type=str, nargs=1, help='If defined, becomes the ylabel of the plot')
parser.add_argument('--y2label', type=str, nargs=1, help='If defined, becomes the ylabel of the 2nd plot')
parser.add_argument('--title', type=str, nargs=1, help='If defined, becomes the title of the plot')
parser.add_argument('--loglog', action='store_true', help='If set, plots loglog plot.  This is the default')
parser.add_argument('--semilogx', action='store_true', help='If set, plots semilogx plot.')
parser.add_argument('--semilogy', action='store_true', help='If set, plots semilogy plot.')
parser.add_argument('--linear', action='store_true', help='If set, plots linear plot.')
parser.add_argument('--dB', action='store_true', help='If set, converts from dB to magnitude.')
parser.add_argument('--openPlot', action='store_true', help='If set, opens the plot you just created')
args = parser.parse_args()

if args.xlabel is not None:
  xAxisLabel = args.xlabel[0]
if args.ylabel is not None:
  yAxisLabel = args.ylabel[0]
if args.y2label is not None:
  y2AxisLabel = args.y2label[0]
if args.title is not None:
  plotTitle = args.title[0]
print 'xlabel =  ', xAxisLabel
print 'ylabel =  ', yAxisLabel
print 'y2label = ', y2AxisLabel
print 'title =   ', plotTitle
print 

# If the user has selected something other than a loglog plot
if args.semilogx is True:
  loglog = False
  semilogx = True
if args.semilogy is True:
  loglog = False
  semilogy = True
if args.linear is True:
  loglog = False

# If measurement is in dB instead of magnitude
if args.dB is True:
  isdB = True
if args.openPlot is True:
  openPlot = True

# Make nice plot legend labels from the data filename
labels = np.array([])
print 'Legend labels:'
for tempFile in vars(args)['files']:
  name = tempFile.name.replace('_', ' ')
  name = name[:-4]
  nameList = name.split('/')
  labels = np.append(labels, nameList[-1])
  print labels[-1]
print

# Check if equivalent /plots/ directory exists where the /data/ folder is
curDateLabel = time.strftime("%b-%d-%Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

pathToData = os.path.dirname(os.path.abspath(tempFile.name)) + '/'
pathToPlots = pathToData.replace('/data/', '/plots/')

plotSaveName = os.path.basename(tempFile.name).replace('.txt', '') # plotSaveName is always the same as the last .txt file
plotSaveName = curDate + '_' + plotSaveName

print 'Data is in ', pathToData
print 'Plots will be in ', pathToPlots
print 'Plot filename: ', plotSaveName
if not os.path.exists(pathToPlots):
  os.makedirs(pathToPlots)

plotDict = {}
for ii, arg in enumerate(args.files):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

# If data is in dB instead of magnitude, set it to magnitude
if isdB:
  for key in plotDict.keys():
    plotDict[key][:,1] = 10.0**(plotDict[key][:,1] / 20.0)

# Nice colors
tableau20 = [(31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138),
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
 
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

if plotPhase == False:
  h = plt.figure(figsize=(16,12))
  ax = h.gca()
  maxRange = -np.inf
  minRange = np.inf
  maxDomain = -np.inf
  minDomain = np.inf
  for key in plotDict:
    tempFreq = plotDict[key][:,0]
    tempSpec = plotDict[key][:,1]
    
    if loglog == True:
      plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    elif semilogx == True:
      plt.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    elif semilogy == True:
      plt.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    else:
      plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    if max(tempSpec) > maxRange:
      maxRange = max(tempSpec)
    if min(tempSpec) < minRange:
      minRange = min(tempSpec)
    if max(tempFreq) > maxDomain:
      maxDomain = max(tempFreq)
    if min(tempFreq) < minDomain:
      minDomain = min(tempFreq)
  ax.grid()
  plt.xlim(minDomain, maxDomain)
  plt.ylim(minRange, maxRange)
  plt.legend(loc='best')
  ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
  plt.savefig(pathToPlots + curDate +'_'+ plotSaveName +'.pdf', bbox_inches='tight', pad_inches=0.2)
else:
  h = plt.figure(figsize=(16,12))
  f1 = h.add_subplot(211)
  f2 = h.add_subplot(212)
  max1Range = -np.inf
  min1Range = np.inf
  max2Range = -np.inf
  min2Range = np.inf
  maxDomain = -np.inf
  minDomain = np.inf
  for key in plotDict:
    tempFreq = plotDict[key][:,0]
    tempSpec = plotDict[key][:,1]
    tempPhas = plotDict[key][:,2]
    if loglog == True:
      f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    elif semilogx == True:
      f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    elif semilogy == True:
      f1.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    else:
      f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    f2.semilogx(tempFreq, tempPhas, lw=3, color=tableau20[key], alpha=0.75)
    if max(tempSpec) > max1Range:
      max1Range = max(tempSpec)
    if min(tempSpec) < min1Range:
      min1Range = min(tempSpec)

    if max(tempPhas) > max2Range:
      max2Range = max(tempPhas)
    if min(tempPhas) < min2Range:
      min2Range = min(tempPhas)

    if max(tempFreq) > maxDomain:
      maxDomain = max(tempFreq)
    if min(tempFreq) < minDomain:
      minDomain = min(tempFreq)
  
  f1.set_xlim([minDomain, maxDomain])
  f2.set_xlim([minDomain, maxDomain])
  f1.set_ylim([min1Range, max1Range])
  f2.set_ylim([min2Range, max2Range])
  f2.set_yticks([-180, -90, 0, 90, 180])
  f1.grid(which='minor')
  f1.set_axisbelow(True)
  f2.grid(which='minor')
  f2.set_axisbelow(True)
  f1.set_ylabel(yAxisLabel)
  f2.set_ylabel(y2AxisLabel)
  f2.set_xlabel(xAxisLabel)
  f1.legend(loc='best')
  f1.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
  #plt.savefig('./'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
  plt.savefig(pathToPlots + plotSaveName +'.pdf',bbox_inches='tight',pad_inches=0.2)

if openPlot:
  command = 'open ' + pathToPlots + plotSaveName + '.pdf'
  print 'Running: '
  print command
  os.system(command)
