import numpy as np
import re
import os
import time
from ezca import Ezca
import argparse
# Read in all acromag files from the /db/ folder

#parser = argparse.ArgumentParser(description='Writes channels from supplied ChannelDump.txt')

#parser.add_argument('ChannelDump', type=argparse.FileType('r'), nargs=1)
#parser.add_argument('--verbose', type=bool, nargs=1)

#args = parser.parse_args()

print '################################################################'
print '# Make sure to run on computer with access to acromag channels #'
print '################################################################'
print ' Run this script in the background: python VacCanTempMon.py &' 

curDate = time.strftime('%Y%m%d')
humanStartTime = time.strftime('%Y%m%d_%H%M%S')
pathToSaveFolder = '../data/' + curDate + '_VacCanTemperatureMonitor/'
if not os.path.exists(pathToSaveFolder):
  os.makedirs(pathToSaveFolder)
filename = pathToSaveFolder + humanStartTime + '_VacCanTemperatureMonitor.txt'

startTime = time.time()
with open(filename, "w") as myfile:
  myfile.write('# Start of monitoring time = ' + humanStartTime +'\n'
              +'# Time From Start [s], Temp Mon [V]\n')

ez = Ezca(ifo=None)
tempChannel = ['C3:PSL-VACCAN_TEMP', 'C3:PSL_VACCAN_TEMP_OOL', 'C3:PSL-ENV_TEMP_MON', 'C3:PSL-ACTUATOR_AC_MON'] 

# Run an infinite loop of recording each second
while(1):
  with open(filename, "a") as myfile:
    tempValue = [0 for i in range(0,3)]  
    for n in range(0,3):
      tempValue[n] = ez.read(tempChannel[n])
    myfile.write(str(time.time()-startTime) + ' ' + str(tempValue[0]) + ',' + str(tempValue[1]) + ',' + str(tempValue[2]) + ',' + str(tempValue[3]) + '\n')
  time.sleep(1)
   
