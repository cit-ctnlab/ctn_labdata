import numpy as np
import matplotlib as mpl 
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12, 
                     'font.size': 34, 
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24, 
                     'axes.labelsize': 24, 
                     'xtick.color':'k',
                     'xtick.labelsize': 24, 
                     'ytick.color':'k',
                     'ytick.labelsize': 24, 
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24, 
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt 
import argparse
import time
import os


parser = argparse.ArgumentParser(description='Usage: python poleDelayFit.py TF.txt\nFits a pole and a delay to a TF.txt file using simple python libraries')
parser.add_argument('TF', type=argparse.FileType('r'), nargs=1)

args = parser.parse_args()

dataFile = args.TF[0]
pathParts = dataFile.split('/')
tempFolder = './'
for pathPart in pathParts[:-1]: # Loop over everything but the last part
  print pathPart
  tempFolder += tempFolder + pathPart + '/'
saveFolder = tempFolder
print saveFolder
# Load the data
plotDict = np.loadtxt(dataFile)

freq = plotDict[:,0]
mag = plotDict[:,1]
phase = np.pi/180.0*plotDict[:,2] # Plot was already in degrees for some reason

def poleDelay(freq, pole, delay):
  return np.exp(1j*2*np.pi*freq*delay)/(1+ 1j*freq/pole)

from scipy.optimize import curve_fit





