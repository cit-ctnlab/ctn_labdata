This is a folder for scratch books and other quick workings and data plotting.  This is a more dynamic exploration of data. We should try to do most of our serious plotting from IRIS scripts where we can and commit to the plotting directory.

Remember to commit jupyter notebooks with output cells cleared.  Gitlab CI will send you a fail mail if it finds notebooks with guff.
