from measTransRIN import measTransRIN
import time
import os
os.chdir('./Data/')
ii = 0
while(ii<1000):
    try:
        print()
        print("___________________________________________")
        print("Measurement", ii)
        print("___________________________________________")
        measTransRIN(numAvg=1)
        ii = ii + 1
    except BaseException as e:
        print("Error: ", e)
        print("Retrying in 1 min")
        time.sleep(60)
