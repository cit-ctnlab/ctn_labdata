'''
spanParamSpace
This code will span all parameter space for s particular path (North or South)
and will store beatnote spectrum data for them. This could be later analyzed
to find optimum working point.
'''

from BNSpec import BNspec
from gainCycle import gainCycle
import argparse
import time
from epics import caget, caput
import os

NFSSCOMCH = "C3:PSL-NCAV_FSS_COMGAIN_DB"
NFSSFASTCH = "C3:PSL-NCAV_FSS_FASTGAIN_DB"
SFSSCOMCH = "C3:PSL-SCAV_FSS_COMGAIN_DB"
SFSSFASTCH = "C3:PSL-SCAV_FSS_FASTGAIN_DB"

def main(args):
    iniNFSSCOM = caget(NFSSCOMCH)
    iniNFSSFAST = caget(NFSSFASTCH)
    iniSFSSCOM = caget(SFSSCOMCH)
    iniSFSSFAST = caget(SFSSFASTCH)

    stepNCOM = 0
    while(stepNCOM < args.nosNCOM):
        NFSSCOM = args.NFSSCOM_Start + stepNCOM*args.NFSSCOM_Step
        stepNFAST = 0
        while(stepNFAST < args.nosNFAST):
            NFSSFAST = args.NFSSFAST_Start + stepNFAST*args.NFSSFAST_Step
            stepSCOM = 0
            while(stepSCOM < args.nosSCOM):
                SFSSCOM = args.SFSSCOM_Start + stepSCOM*args.SFSSCOM_Step
                stepSFAST = 0
                while(stepSFAST < args.nosSFAST):
                    SFSSFAST = args.SFSSFAST_Start + stepSFAST*args.SFSSFAST_Step
                    print('Attempting beatnote spectrum measurement with:')
                    print('NFSS COM Gain:', round(NFSSCOM), 'dB')
                    print('NFSS FAST Gain:', round(NFSSFAST), 'dB')
                    print('SFSS COM Gain:', round(SFSSCOM), 'dB')
                    print('SFSS FAST Gain:', round(SFSSFAST), 'dB')
                    caput(NFSSCOMCH, NFSSCOM)
                    caput(NFSSFASTCH, NFSSFAST)
                    caput(SFSSCOMCH, SFSSCOM)
                    caput(SFSSFASTCH, SFSSFAST)
                    gainCycle(comCH=SFSSCOMCH, fastCH=SFSSFASTCH)
                    gainCycle(comCH=NFSSCOMCH, fastCH=NFSSFASTCH)
                    time.sleep(10)
                    fList = BNspec(detector=args.detector,
                                   instrument=args.instrument,
                                   fileRoot=args.fileRoot,
                                   ipAddress=args.ipAddress,
                                   duration=args.duration,
                                   sampleRate=args.sampleRate,
                                   bandWidth=args.bandWidth, atten=args.atten,
                                   paramFile=args.paramFile,
                                   chListFile=args.chListFile)
                    # delete csv file to save disk space
                    os.remove(fList[0])
                    stepSFAST = stepSFAST + 1
                stepSCOM = stepSCOM + 1
            stepNFAST = stepNFAST + 1
        stepNCOM = stepNCOM + 1
    caput(NFSSCOMCH, iniNFSSCOM)
    caput(NFSSFASTCH, iniNFSSFAST)
    caput(SFSSCOMCH, iniSFSSCOM)
    caput(SFSSFASTCH, iniSFSSFAST)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This scrip changes FSS gains and takes beatnote spectrum '
                    'to see if any noise change happens in the experiment.')
    parser.add_argument('--instrument', type=str,
                        help='Moku or SR785',
                        default='Moku')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku or SR785',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is veryfast.',
                        default='veryfast')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('-c', '--chListFile', type=str,
                        help='ChannelList file with path to write in logs.',
                        default='ChannelList.txt')
    parser.add_argument('--paramFile', nargs='?',
                        help='The parameter file for the measurement by SR785',
                        default='CTN_BNSpec_SR785.yml')
    parser.add_argument('-f', '--fileRoot', type=str,
                        help='Fileroot to all data and configuration files'
                             'Default is Beatnote.',
                        default='Beatnote')
    parser.add_argument('--NFSSCOM_Start', type=int,
                        help='Starting COMMON Gain value of NFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--NFSSFAST_Start', type=int,
                        help='Starting FAST Gain value of NFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--SFSSCOM_Start', type=int,
                        help='Starting COMMON Gain value of SFSS in dB.'
                             'Default is 11.',
                        default=11)
    parser.add_argument('--SFSSFAST_Start', type=int,
                        help='Starting FAST Gain value of SFSS in dB.'
                             'Default is 5.',
                        default=5)
    parser.add_argument('--NFSSCOM_Step', type=int,
                        help='Stepping COMMON Gain value of NFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--NFSSFAST_Step', type=int,
                        help='Stepping FAST Gain value of NFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--SFSSCOM_Step', type=int,
                        help='Stepping COMMON Gain value of SFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--SFSSFAST_Step', type=int,
                        help='Stepping FAST Gain value of SFSS in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosNCOM', type=int,
                        help='Number of steps in NFSS COM gain stepping.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosSCOM', type=int,
                        help='Number of steps in SFSS COM gain stepping.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosNFAST', type=int,
                        help='Number of steps in NFSS FAST gain stepping.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosSFAST', type=int,
                        help='Number of steps in SFSS FAST gain stepping.'
                             'Default is 1.',
                        default=1)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    args.instrument = args.instrument.lower()
    if args.instrument != 'moku' and args.instrument != 'sr785':
        raise RuntimeError('Instrument can be Moku or SR785 only.')
    main(args)
