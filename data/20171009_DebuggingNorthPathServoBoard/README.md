This folder contains point1/point2 transfer functions taken with a pair of HP 41800A probes.  This is an effort to debug the source of loop locking issues and improve badwidth of loops.

Note that Ser5 refers to serial board number 5 that is the north path field box and Ser6 refers to the field box that was at the 40 m.
