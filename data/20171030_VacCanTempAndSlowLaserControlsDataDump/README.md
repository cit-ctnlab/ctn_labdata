Data collected on 20171030

This folder contains a few hours data logged using StripTool of the CTN vacuum can temperature sensors and the laser slow controls. It shows drift of the laser slow voltages over time and the long term infered temperatures from the AD590 sensors at four sensors placed on the vacuum can.  The sensors are placed symetrically but only one is used to feed back to the resistive heaters.
