This data is a comparison of the ISS impact on intensity noise (on transmission of the cavities) in each cavity.

An out of loop sensor was placed on the unused port of the post refcav BN beamsplitter and a set of frequency spans were taken using the SR785.

The ISS were configured as follows:
- Thorlabs PDA10CS sensing transmission pick off of each PD (both were set to 30 dB gain with about 3 V average value
- SR560 set to AC coupling, high pass 10 Hz (6 dB/oct), low pass 100Hz (6 dB/oct), high dynamic range, gain 1x10^4 north and 5x10^4 south.  Both were powered from mains.
- 50 ohms out was then put into AEOMs in the north and south path tuned to 75 % of their maximum
- Both cavities are locked (obviously)
- read was made from a PD (Thorlabs PDA10CS) set to 20 dB gain (about 3 V)
