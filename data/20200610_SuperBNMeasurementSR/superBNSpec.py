from BNSpec import BNspec
from mokuCleanRAM import mokuCleanRAM
import os
import time
from transRINOK import transRINOK

waitTime = 60*25
ii = 0
while(ii < 10000):
    if transRINOK():
        dataDir = time.strftime("./Data%m%d/")
        if not os.path.exists(dataDir):
            os.mkdir(dataDir)
        cwd = os.getcwd()
        os.chdir(dataDir)
        try:
            print('Measurement Number', ii)
            startTime = time.time()
            fList = BNspec(detector='SN101',
                           instrument='moku',
                           fileRoot='Beatnote',
                           ipAddress='10.0.1.81',
                           duration=960,
                           sampleRate='veryfast',
                           bandWidth=10e3,
                           atten=False,
                           paramFile='CTN_BNSpec_SR785.yml',
                           chListFile='/home/controls/Git/cit_ctnlab'
                                      '/ctn_scripts/BNSpecLogChannelList.txt',
                           measRIN=True,
                           reduceTSfile=True)
            os.remove(fList[0].replace('.txt', '.csv'))
            mokuCleanRAM(ipAddress='10.0.1.81')
            curTime = time.time()
            time.sleep(max(0, startTime + waitTime - curTime))
            ii = ii + 1
        except BaseException as e:
            print('Error occured:', e)
            print('Cleaning RAM.')
            mokuCleanRAM(ipAddress='10.0.1.81')
            print('Retrying in 1 min.')
            time.sleep(60)
            ii = ii - 1
        os.chdir(cwd)
