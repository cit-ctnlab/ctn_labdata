This folder contains two transfer funtions of the SR560 with the acromag plugged into the 600 ohm port and with it not plugged in.  A 1 mV signal is used to excit the B port, this should be a flat transfer function. With the loading of the acromag the gain changes greatly.

This is the source of some strange artifacts in calibrating final noise spectrum. It apears to pull the UGF of the PLL down to around 8-9 kHz which makes measurments around that decade dubious.
