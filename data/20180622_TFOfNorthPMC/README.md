Andrew Wade and Anchal Gupta

June 22, 2018

Data taken for the transfer function of the PMC closed loop response. Measured made from J4 to J8 of the PMC servo board (LIGO-D980352) first with the loop closed and then opened.  

It turns out that an error was made in the EPICS channel convertion from medm slider db units to voltage at the AD602.  The low pass filter is a gain 0.1.  So gains of the slider were wrong by a lot.  This ment that even for the '30db' setting we were seeing UGF of only 200 Hz ish.

Further more the LP filter on the final stage at the output of the HV amp was probably about 300 Hz based on the 1 kohm output impedance and the 500 nF of the PZT capacitance.  So the loop essentially isn't low pass filtered at the ussual 10 Hz which would give it more stablity.

We will add a pamona box LPF soon.
