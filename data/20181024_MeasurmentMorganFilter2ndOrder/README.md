This folder contains data and plots of first build of Morgan style reflectionless RF filter.

This particular filter is a symetric diplexer with two stages.  Inductor values are 270 nH and capacitors are 68 pF.  Resistors used were 51 ohm (as that was all that was available) and are only thick film type.  Don't think they are rated above 1/5 W.

Data shows a clear dip at 36.5 MHz that is > 75 dB attenuation.  The width of the dip is such that attenuation is better than 60 dB over the interval 35.3 MHz - 37.5 MHz.  This would make it suitable for use in both north and south paths without modification.  Although we would probaly want to fine trim it with a few small value capacitors.

Data is taken for sweep 1 Mhz to 200 MHz and then again closer around the peak.
