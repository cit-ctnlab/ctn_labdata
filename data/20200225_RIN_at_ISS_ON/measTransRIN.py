from SRmeasure import main as SRmeasure
from epics import caget

STransDCCH = 'C3:PSL-SCAV_TRANS_DC'
NTransDCCH = 'C3:PSL-NCAV_TRANS_DC'

for ii in range(1000):
    SDCval = caget(STransDCCH)
    NDCval = caget(NTransDCCH)
    header = ('North DC Val: ' + str(NDCval) + ' Volts'
              + 'South DC Val: ' + str(SDCval) + ' Volts')
    SRmeasure(paramFile='TransSpec.yml', extraHeader=header)
