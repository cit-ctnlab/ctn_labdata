This data is the PLL spectrum pre-scatter diagnostics so that we have a reference point for the state of the experiment before addressing LF scatter.

Next step after this is to buzz mounts and clean up sources of scatter.

Config of FSS
South FSS settings
Common Gain	1.1329 V
Fast Gain	1.79449 V
Offset	-0.02871 V
UGF	115 kHz
Crossover Freq	10.27 kHz

North FSS settings
Common Gain	0.64040 V
Fast Gain	1.59204 V
Offset	-0.38 V
UGF	125.4 kHz
Crossover Freq	9.92 kHz

PLL UGF = 34 kHz
