from SRmeasure import main as SRmeasure
from epics import caget
import os

SReflDCCH = 'C3:PSL-SCAV_REFL_DC'
NReflDCCH = 'C3:PSL-NCAV_REFL_DC'

for ii in range(500):
    try:
        DCval = caget(SReflDCCH)
        header = 'DC Val: ' + str(DCval)
        fn1 = SRmeasure(paramFile='TransSpec1_S.yml',
                        extraHeader=header)
        fn2 = SRmeasure(paramFile='TransSpec2_S.yml')
        with open(fn1, 'r') as f:
            fn1lines = f.readlines()
        with open(fn2, 'r') as f:
            fn2lines = f.readlines()
        allLines = (fn1lines
                    + ['#\n', '#\n']
                    + fn2lines[:30]
                    + fn2lines[37:])
        os.remove(fn1)
        os.remove(fn2)
        with open(fn1, 'w') as f:
            f.writelines(allLines)
    except BaseException:
        print('Error occured. Waiting 10s.')
        ii = ii - 1
        print('Continuing now.')
