This is a retake of the previous day's transfer functions of the south path board.  This time the power injected at TP1 was reduced to -23 dBm. Also a 50 ohm terminator was inserted before the test clips to give true voltage.

Still looks saturated in the later stages.
