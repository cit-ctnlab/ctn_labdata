
loadtxt = dlmread('~/Git/ctn_labdata/data/20170821_PLL_OLGTFs/PLL_OLGTFs_21-08-2017_114453.txt', '',16,0);

freq = loadtxt(:,1);
PLLmag = loadtxt(:,2);
PLLphase = loadtxt(:,3); % This is in degrees

startIndex = 300;
endIndex = 700;
shortFreq = freq(startIndex:endIndex);
PLL = PLLmag(startIndex:endIndex) .* exp(1i*PLLphase(startIndex:endIndex)*pi/180.0);
PLLfrd = frd(PLL, shortFreq, 'FrequencyUnit','Hz');

poleNumber = 1;
zeroNumber = poleNumber - 1;
PLLest = tfest(PLLfrd, poleNumber, zeroNumber)
figure(2)
bode(PLLest)

testFreq = logspace(0,6);
PLLestFreqResp = squeeze(freqresp(PLLest, testFreq, 'Hz')) .* exp(-1j*2*pi.*testFreq(:)/6.0e5);
PLLestMag = abs(PLLestFreqResp);
PLLestPhase = angle(PLLestFreqResp);

figure(1);
subplot(211);
loglog(freq, PLLmag); hold on; grid on;
loglog(shortFreq, abs(PLL));
loglog(testFreq, PLLestMag);
ylabel('PLL Mag')
title('PLL OLG TF Analytic Estimate')
legend('Meas PLL','Estimated TF')

subplot(212);
semilogx(freq, PLLphase); hold on; grid on;
semilogx(shortFreq, 180/pi*angle(PLL));
semilogx(testFreq, 180/pi*PLLestPhase);
ylabel('PLL OLG Phase [deg]')
xlabel('Frequency [Hz]')
