'''
spanParamSpace
This code will span all parameter space for a particular path (North or South)
and will store beatnote spectrum data for them. This could be later analyzed
to find optimum working point.
'''

from BNSpec import BNspec
from gainCycle import gainCycle
import argparse
import time
from epics import caget, caput
import os

NPMCCH = "C3:PSL-NCAV_PMC_MGAIN_DB"
SPMCCH = "C3:PSL-SCAV_PMC_MGAIN_DB"

def main(args):
    iniNPMC = caget(NPMCCH)
    iniSPMC = caget(SPMCCH)

    stepNPMC = 0
    while(stepNPMC < args.nosNPMC):
        NPMC = args.NPMC_Start + stepNPMC*args.NPMC_Step
        stepSPMC = 0
        while(stepSPMC < args.nosSPMC):
            SPMC = args.SPMC_Start + stepSPMC*args.SPMC_Step
            print('Attempting beatnote spectrum measurement with:')
            print('NPMC Gain:', round(NPMC), 'dB')
            print('SPMC Gain:', round(SPMC), 'dB')
            caput(NPMCCH, NPMC)
            caput(SPMCCH, SPMC)
            time.sleep(10)
            fList = BNspec(detector=args.detector,
                           instrument=args.instrument,
                           fileRoot=args.fileRoot,
                           ipAddress=args.ipAddress,
                           duration=args.duration,
                           sampleRate=args.sampleRate,
                           bandWidth=args.bandWidth, atten=args.atten,
                           paramFile=args.paramFile,
                           chListFile=args.chListFile)
            # delete csv file to save disk space
            os.remove(fList[0])
            stepSPMC = stepSPMC + 1
        stepNPMC = stepNPMC + 1
    caput(NPMCCH, iniNPMC)
    caput(SPMCCH, iniSPMC)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This scrip changes PMC gains and takes beatnote spectrum '
                    'to see if any noise change happens in the experiment.')
    parser.add_argument('--instrument', type=str,
                        help='Moku or SR785',
                        default='Moku')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku or SR785',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is veryfast.',
                        default='veryfast')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('-c', '--chListFile', type=str,
                        help='ChannelList file with path to write in logs.',
                        default='ChannelList.txt')
    parser.add_argument('--paramFile', nargs='?',
                        help='The parameter file for the measurement by SR785',
                        default='CTN_BNSpec_SR785.yml')
    parser.add_argument('-f', '--fileRoot', type=str,
                        help='Fileroot to all data and configuration files'
                             'Default is Beatnote.',
                        default='Beatnote')
    parser.add_argument('--NPMC_Start', type=int,
                        help='Starting COMMON Gain value of NPMC in dB.'
                             'Default is -10.',
                        default=-10)
    parser.add_argument('--SPMC_Start', type=int,
                        help='Starting COMMON Gain value of SPMC in dB.'
                             'Default is -10.',
                        default=-10)
    parser.add_argument('--NPMC_Step', type=int,
                        help='Stepping COMMON Gain value of NP<C in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--SPMC_Step', type=int,
                        help='Stepping COMMON Gain value of SPMC in dB.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosNPMC', type=int,
                        help='Number of steps in NPMC gain stepping.'
                             'Default is 1.',
                        default=1)
    parser.add_argument('--nosSPMC', type=int,
                        help='Number of steps in SPMC gain stepping.'
                             'Default is 1.',
                        default=1)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    args.instrument = args.instrument.lower()
    if args.instrument != 'moku' and args.instrument != 'sr785':
        raise RuntimeError('Instrument can be Moku or SR785 only.')
    main(args)
