After comment in PSL:1888 from rana that the EOM notch on the south path is wrong, I measured the Out2/EXC common path CL TF again.  Looking at the out1/out2 zoomed in around 500kHz-5MHz range, I then tuned the EOM notch on the servo board (inside the FSS field box). C13 was tuned with a screwdriver until the notch actually canceled the EOM resonance. -awade

The data for the supression function is all bad: I failed to plug the excitation in.
