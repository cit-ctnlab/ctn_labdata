import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'lines.linewidth': 4,
                     'font.size': 24,
                     'font.family': 'FreeSerif',
                     'figure.figsize': (16, 12),
                     'xtick.labelsize': 24, 
                     'ytick.labelsize': 24,
                     'axes.grid': True,
                     'grid.color': '#555555',
                     'lines.markersize': 12})
import matplotlib.pyplot as plt
import argparse
import time

loglog = True # Set if we would like a loglog plot
semilogx = False
xAxisLabel = 'Frequency [Hz]'
yAxisLabel = 'Magnitude'

plotPhase = True # Set if there is another row with phase information
if plotPhase == True:
  y2AxisLabel = 'Phase [degs]' # Set y2 axis label
isCsv = False # Set if reading in .csv files
plotTitle = '35.5 MHz RFPDs'
plotSaveName = '35p5MHz_RFPD_TFs'

parser = argparse.ArgumentParser(description='Usage: python makeNicePlots.py *.txt\nThis will pull all .txt files in this directory and plot them nicely.')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+')

args = parser.parse_args()

labels = np.array([])
for tempFile in vars(args)['files']:
  labels = np.append(labels, tempFile.name.replace('_', ' '))
  print tempFile.name

curDateLabel = time.strftime("%b-%d-%Y %H:%M:%S")
curDate = time.strftime("%b-%d-%Y_%H%M%S")

plotDict = {}
for ii, arg in enumerate(args.files):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

tableau20 = [(31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138), 
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
 
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

#bandwidth = plotDict[0][1,0] - plotDict[0][0,0]
#NENBW = 1.5 #Hanning Window



if plotPhase == False:
  h = plt.figure(figsize=(16,12))
  ax = h.gca()
  maxRange = -np.inf
  minRange = np.inf
  maxDomain = -np.inf
  minDomain = np.inf
  for key in plotDict:
    tempFreq = plotDict[key][:,0]
    tempSpec = plotDict[key][:,1]
    
    if loglog == True:
      plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    else:
      plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    if max(tempSpec) > maxRange:
      maxRange = max(tempSpec)
    if min(tempSpec) < minRange:
      minRange = min(tempSpec)
    if max(tempFreq) > maxDomain:
      maxDomain = max(tempFreq)
    if min(tempFreq) < minDomain:
      minDomain = min(tempFreq)
  ax.grid()
  plt.xlim(minDomain, maxDomain)
  plt.ylim(minRange, maxRange)
  plt.legend(loc='best')
  ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
  plt.savefig('./'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
else:
  h = plt.figure(figsize=(16,12))
  f1 = h.add_subplot(211)
  f2 = h.add_subplot(212)
  max1Range = -np.inf
  min1Range = np.inf
  max2Range = -np.inf
  min2Range = np.inf
  maxDomain = -np.inf
  minDomain = np.inf
  for key in plotDict:
    tempFreq = plotDict[key][:,0]
    tempSpec = plotDict[key][:,1]
    tempPhas = plotDict[key][:,2]
    if loglog == True:
      f1.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    elif semilogx == True:
      f1.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    else:
      f1.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
    f2.semilogx(tempFreq, tempPhas, lw=3, color=tableau20[key], alpha=0.75)
    if max(tempSpec) > max1Range:
      max1Range = max(tempSpec)
    if min(tempSpec) < min1Range:
      min1Range = min(tempSpec)

    if max(tempPhas) > max2Range:
      max2Range = max(tempPhas)
    if min(tempPhas) < min2Range:
      min2Range = min(tempPhas)

    if max(tempFreq) > maxDomain:
      maxDomain = max(tempFreq)
    if min(tempFreq) < minDomain:
      minDomain = min(tempFreq)
  
  f1.set_xlim([minDomain, maxDomain])
  f2.set_xlim([minDomain, maxDomain])
  f1.set_ylim([min1Range, max1Range])
  f2.set_ylim([min2Range, max2Range])
  f2.set_yticks([-180, -90, 0, 90, 180])
  f1.grid(which='minor')
  f1.set_axisbelow(True)
  f2.grid(which='minor')
  f2.set_axisbelow(True)
  f1.set_ylabel(yAxisLabel)
  f2.set_ylabel(y2AxisLabel)
  f2.set_xlabel(xAxisLabel)
  f1.legend(loc='best')
  f1.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
  plt.savefig('./'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
  plt.savefig('./'+ curDate +'_'+ plotSaveName +'.pdf',bbox_inches='tight',pad_inches=0.2)

