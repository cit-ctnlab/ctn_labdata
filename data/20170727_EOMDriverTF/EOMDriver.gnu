reset
set logscale x
set xrange[707.946:1.41254e+08]
db2abs(x)=10.**(x/20.)
w(x)=180/pi*arg(x)
ww(x)=w(x)
wwp(x)=(w(x)>=0)?w(x):w(x)+360
wwm(x)=(w(x)<=0)?w(x):w(x)-360
abs2db(x)=20.*log10(abs(x))
i={0,1}
set xlabel "Frequency [Hz]"
set ylabel "Abs "
set y2label "Phase [Degree]"
set ytics nomirror
set y2tics nomirror
set y2tics -90,15,90
set my2tics 3
set y2range [-90:90]
set mxtics 10
set mytics 10
z(x,y)=x+i*y
set key outside
set log y
set output "EOMDriver.pdf"
set term pdf font "Times,12"
set size ratio 0.6
set key below
set format y "%g"
set grid xtics ytics
plot\
"EOMDriver.out" using ($1):($2) axes x1y1 title "U(nout) [V/A] Abs " w l lt 1 lw 2,\
"EOMDriver.out" using ($1):($3) axes x1y2 title "U(nout) [V/A] Phase " w l lt 2 lw 2
