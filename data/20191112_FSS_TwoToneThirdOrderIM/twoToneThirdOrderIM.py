from AGmeasure import main as AGmeasure
from mokuWG import mokuWG
import argparse
import numpy as np
import os

defaultPowArr = np.linspace(-40,-20,5)

def twoToneThirdOrderIM(paramFile, filename, CF, df, powArray=defaultPowArr,
                        attenuation=0):
    for pow in powArray:
        sourceVpp = 2*(10**((pow+attenuation-10)/20))
        mokuWG(chan='both', freq=[CF-df/2, CF+df/2], amp=sourceVpp, wf='sine')
        AGmeasure(paramFile=paramFile,
                  filename=filename,
                  extraHeader='Source Power ' + str(pow) + 'dBm')

def grabInputArgs():
    parser = argparse.ArgumentParser(
                        description='This script sends two tones'
                                    'using Moku and reads spectrum using'
                                    'AG4395A.'
    )
    parser.add_argument('paramFile', nargs='?',
                        help='The parameter file for the measurement in'
                             'AG4395A.',
                        default=None)
    parser.add_argument('-f', '--filename', help='Stem of output filename.'
                        'Overrides parameter file.', default=None)
    parser.add_argument('-c', '--centralFreq', type=float,
                        help='Central Frequency. Default is 36 MHz.',
                        default=36e6)
    parser.add_argument('-d', '--diffFreq', type=float,
                        help='Difference Frequency. Default is 10 kHz',
                        default=10e3)
    parser.add_argument('--powStart', type=float,
                        help='Start source power level in dBm.'
                             'Default -40 dBm.',
                        default=-40)
    parser.add_argument('--powStop', type=float,
                        help='Stop source power level in dBm. This power will '
                             'not reach.'
                             'Default -20 dBm.',
                        default=-20)
    parser.add_argument('--powStep', type=float,
                        help='Step source power level in dBm.'
                             'Default 5 dBm.',
                        default=5)
    parser.add_argument('-a', '--attenuation', type=float,
                        help='Attenuation added to source if any. '
                             'Source power will be increased accordingly '
                             'to match the required power after combination. '
                             'Default is 3 for an ideal combiner.',
                        default=3)
    return parser.parse_args()

if __name__ == "__main__":
    args = grabInputArgs()
    powArr = np.arange(args.powStart, args.powStop, args.powStep)
    twoToneThirdOrderIM(args.paramFile, args.filename, args.centralFreq,
                        args.diffFreq, powArr, args.attenuation)
