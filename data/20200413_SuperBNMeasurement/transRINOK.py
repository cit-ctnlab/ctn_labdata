from epics import caget
import numpy as np
import time
timestep = 0.1
averagingTime = 1    # Averaging time for slop calculations
recLen = int(2*averagingTime/timestep)
thresholdN = 0.005
thresholdS = 0.005


def transRINOK(verbose=False):
    NTransPow = np.zeros(recLen)
    STransPow = np.zeros(recLen)
    for ii in range(recLen):
        NTransPow[ii] = caget('C3:PSL-NCAV_TRANS_POW')
        STransPow[ii] = caget('C3:PSL-SCAV_TRANS_POW')
        time.sleep(timestep)
    NTransRIN = NTransPow/np.mean(NTransPow)
    STransRIN = STransPow/np.mean(STransPow)
    stdN = np.std(NTransRIN)
    stdS = np.std(STransRIN)
    if verbose:
        print('North Trans Power RIN Standard Dev.:', stdN)
        print('South Trans Power RIN Standard Dev.:', stdS)
    return (stdN < thresholdN) and (stdS < thresholdS)

if __name__ == "__main__":
    while(1):
        print(transRINOK(verbose=True))
