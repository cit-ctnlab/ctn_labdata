Retake of BN spectrum after reinstalling green glass beam dump in north path.

Installed green glass beam dump in south path, flipped polarization from s-pol to p-pol.  Adjusted angle of detector from 40 degree to 30 degree.

There are still a few dots around the PD from spruious sources.
