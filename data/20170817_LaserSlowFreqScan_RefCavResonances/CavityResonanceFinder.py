import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34, 
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import re
import time
import os
from ezca import Ezca
import ConfigParser
import sys
import argparse

# Craig Cahillane 8/16/17
# 

#test if argument is outside and range and rails it to the limit of that range if it goes over
def rail(number, lower_limit, upper_limit):
  if number<lower_limit:
    number = lower_limit
  elif number>upper_limit:
    number = upper_limit
  return(number)

# Read in arguments
parser = argparse.ArgumentParser(description='CavityResonanceFinder.py\n\nUsage:\npython CavityResonanceFinder.py North -10 10 0.00001\nThis looks for the 00 resonance modes for the North cavity between the voltages of -10 and 10 using incremental steps of 0.00001.  You gotta go slow around the resonances because our cavity finesse is so high.')
parser.add_argument('path', type=str, nargs=1, help='Path: Choose which path to sweep the slow laser control voltage. Either "North" or "South"')
parser.add_argument('startVoltage', type=float, nargs=1, help='startVoltage: Choose where the slow laser voltage control begins. Must be a smaller number than endVoltage.')
parser.add_argument('endVoltage', type=float, nargs=1, help='endVoltage: Choose where the slow laser voltage control ends up.')
parser.add_argument('increment', type=float, nargs=1, help='increment: Choose what step size is used.  Steps always happen at 1 Hz.  Must be a positive number.')
args = parser.parse_args()
print args.path[0]
print args.startVoltage[0]
print args.endVoltage[0]
print args.increment[0]

startVoltage = args.startVoltage[0]
endVoltage = args.endVoltage[0]
increment = args.increment[0]
path = args.path[0] #argparse always returns a list.  This gets the first item in that list
if path[0] == 'N' or path[0] == 'n':
  pathName = 'North'
elif path[0] == 'S' or path[0] == 's':
  pathName = 'South'
else:
  print
  print 'Neither the North or South path was selected, using South path automatically in '
  for ii in range(5):
    print str(5-ii)
    sys.stdout.flush()
    time.sleep(1)
  pathName = 'South'

configFileName = pathName + '_WindshieldWiper.ini'

#####Actual Process Code Starts Here#####

RCPID = Ezca(ifo=None)

config = ConfigParser.ConfigParser()
config.read(configFileName)

laserpath = config.get("EPICSChannelConfig", "laserpath")
timestep  = config.get("EPICSChannelConfig", "timestep")
transDC = config.get("EPICSChannelConfig", "transDC")

# Load more actuator limit parameters -- these ones actually have to be numbers
hard_stops = [config.getfloat("HardActuatorLimits","hardstops_upper"),config.getfloat("HardActuatorLimits","hardstops_lower")]

currentVoltage = RCPID.read(laserpath, log=False) 

# Check if startVoltage and endVoltage are valid between rails (-10, 10)
if np.abs(endVoltage - rail(endVoltage, hard_stops[0], hard_stops[1])) > 0.00001: #if not equal
  print('End voltage out of range, must be between '+str(hard_stops[0])+' and '+str(hard_stops[1]) )
  sys.exit()
if np.abs(startVoltage - rail(startVoltage, hard_stops[0], hard_stops[1])) > 0.00001: #if not equal
  print('Start voltage out of range, must be between '+str(hard_stops[0])+' and '+str(hard_stops[1]) )
  sys.exit()

currentTrans = RCPID.read(transDC, log=False)
print('Starting'+ pathName +' Trans DC voltage from transimpedence PD = '+ str(currentTrans))

voltArray = np.array([])
transArray = np.array([])

# Set temp to startVoltage then wait 15 seconds to thermalization
RCPID.write(laserpath, startVoltage, monitor=False)
print('Waiting 15 seconds for thermalization')
time.sleep(15)

currentVoltage = RCPID.read(laserpath, log=False)    
currentTrans = RCPID.read(transDC, log=False) 
voltArray = np.append(voltArray, currentVoltage)
transArray = np.append(transArray, currentTrans)

# Start incrementing
while np.abs(currentVoltage - endVoltage) > 0.0000001:
  start_time = time.time()
 
  # Take measurement of DC trans power at 10 Hz 
  currentVoltage = RCPID.read(laserpath, log=False)  
  currentTrans = RCPID.read(transDC, log=False)
  voltArray = np.append(voltArray, currentVoltage)
  transArray = np.append(transArray, currentTrans)
  
  # Find the next voltage and set the value
  nextVoltage = currentVoltage + increment
  if nextVoltage > endVoltage:
    nextVoltage = endVoltage   
  RCPID.write(laserpath, nextVoltage, monitor=False)
  
  time_elapsed = time.time() - start_time # time since starting this iteration of the while loop
  time.sleep(0.1 - time_elapsed) # Sleep for 0.1 seconds

# Take measurement of DC trans power at 10 Hz 
currentVoltage = RCPID.read(laserpath, log=False)  
currentTrans = RCPID.read(transDC, log=False)
voltArray = np.append(voltArray, currentVoltage)
transArray = np.append(transArray, currentTrans)
  
print('Measurement Done, saving data')
dataTxt = np.vstack([voltArray, transArray]).T
curDate = time.strftime('%d-%b-%Y_%H%M%S')
np.savetxt('../data/'+curDate+'_'+pathName+'LaserSlowControlVoltageVsTransPower.txt', dataTxt, fmt='%.10f', header='Ctrl Volts, Trans DC Volts')

print('Making plot')
h = plt.figure(figsize=(16,12))
f1 = h.add_subplot(111)
f1.plot(voltArray, transArray, 'o', ms=10)
f1.set_title(r''+ pathName +' Laser Slow Control Voltage vs Trans Power')
f1.set_xlabel(r''+ pathName + ' Laser Slow Control Voltage [V]')
f1.set_ylabel(r'Trans DC Power [V]')

plt.savefig('../plots/'+curDate+'_'+pathName+'LaserSlowControlVoltageVsTransPower.pdf', bbox_inches=None, pad_inches=0.1)
