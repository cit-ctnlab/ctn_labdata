
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34,
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 24,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})
import matplotlib.pyplot as plt
import argparse
import time

################################
##### SET THE BELOW PARAMS #####
################################
loglog = False   # Set if we would like a loglog plot
semilogx = False # Set if we want semilogx plot
semilogy = False # Set if we want semilogy plot
# if none of the above are set, we do a regular linear plot

isCsv = False # Set if reading in .csv files
xAxisLabel = 'North Laser Slow Control Voltage [V]'
yAxisLabel = 'North Trans [V]'
plotTitle = 'North Laser Temperature Scan for Resonances'
plotSaveName = 'NorthLaserSlowControlVoltageVsTransPower'
################################

parser = argparse.ArgumentParser(description='Usage: python txtPlotter.py *.txt\nThis will pull all .txt files in this directory and plot them nicely.')
parser.add_argument('files', type=argparse.FileType('r'), nargs='+')

args = parser.parse_args()

labels = np.array([])
for tempFile in vars(args)['files']:
  labels = np.append(labels, tempFile.name.replace('_', ' '))
  print tempFile.name

curDateLabel = time.strftime("%b %d %Y %H:%M:%S")
curDate = time.strftime("%Y%m%d_%H%M%S")

plotDict = {}
for ii, arg in enumerate(args.files):
  if isCsv == False:
    plotDict[ii] = np.loadtxt(arg)
  else:
    plotDict[ii] = np.loadtxt(arg, delimiter=',')

tableau20 = [(31, 119, 180), (255, 127, 14), (44, 160, 44), (152, 223, 138),
      (214, 39, 40), (255, 152, 150), (174, 199, 232),
      (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
      (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
      (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229), (255, 187, 120)]
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

h = plt.figure(figsize=(16,12))
ax = h.gca()
maxRange = -np.inf
minRange = np.inf
maxDomain = -np.inf
minDomain = np.inf
for key in plotDict:
  tempFreq = plotDict[key][:,0]
  tempSpec = plotDict[key][:,1]

  if loglog == True:
    plt.loglog(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  if semilogx == True:
    plt.semilogx(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  if semilogy == True:
    plt.semilogy(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  else:
    plt.plot(tempFreq, tempSpec, lw=3, color=tableau20[key], label=labels[key], alpha=0.75)
  
  if max(tempSpec) > maxRange:
    maxRange = max(tempSpec)
  if min(tempSpec) < minRange:
    minRange = min(tempSpec)
  if max(tempFreq) > maxDomain:
    maxDomain = max(tempFreq)
  if min(tempFreq) < minDomain:
    minDomain = min(tempFreq)
ax.grid(which='minor')
plt.xlim(minDomain, maxDomain)
plt.ylim(minRange, maxRange)
plt.xlabel(xAxisLabel)
plt.ylabel(yAxisLabel)
plt.legend(loc='best')
ax.set_title(plotTitle +' - CTN Lab '+ curDateLabel)
plt.savefig('../plots/'+ curDate +'_'+ plotSaveName +'.pdf',bbox_inches='tight',pad_inches=0.2)
plt.savefig('../plots/'+ curDate +'_'+ plotSaveName +'.png',bbox_inches='tight',pad_inches=0.2)
