This data is the noise and TF of the LF preampifier box made in April 2018 for PLL loop.

The circuit is document in the elog and consists of two LT1128s in parrallel (to reduce voltage noise) and followed by an OP27 unity gain stage to sum the outputs.  Gain on the first pair of LT1128s is 101 (non-inverting R1=1kohm R2=10ohm).  These sum together to give another factor of 2 gain.  This gives total gain of 202.

Noise is measured to be 1.2 nV/rtHz at its lowest point with a 1/f roll up at LF and some peaking going up to 100 kHz.
