We were spectulating that RIN might be adding noise to our measurment.  So I got two SR560 preamps and connected them to the trasmission DC PDs.  I then put an offset with voltage calibrator boxes into the B input to provide ref operating point in the A-B mode.  The output 50 ohms was routed to the AEOMS that had horziontal pol on input and lambda/4 + PBS on output. These were configured to operate at the 50% point of circular pol. at 0 V. 

Although the AEOMS with the SR560s only drove to 3 V, this was about 3-5 % of total range and enough to stablize. We may need to build wider range drivers in future.  Gain on the SR560s was set to x100 and this seem to be ok without railing the output.  A better approch would be to turn the gain on the PDs up to start with (but it is possible this would saturate the preamp.  

I also activated boost on the south path and was able to bring the Vrms down to 105 mVrms.  After some fiddling in the path this was the only 130 mVrms.  

This folder contains some 10kHz/Vrms (Marconi settings) PLL BN measurments at various spans. Gain of PLL SR560 was x200 and a 300 kHz LPF was applied. 
