from AGmeasure import main as AGmeasure
from mokuWG import mokuWG
import argparse
import numpy as np
import os

def twoToneTest(paramFile, filename, f1, f2):
    f1Vol = np.array([0.01, 0.1, 1])
    f2Vol = np.array([0.01, 0.1, 1])
    data = np.zeros((len(f1Vol)*len(f2Vol),13))

    ind = 0
    for ii, V1 in enumerate(f1Vol):
        for jj, V2 in enumerate(f2Vol):
            mokuWG(chan='both', freq=[f1, f2], amp=[V1, V2], wf='sine')
            thisFilename = (filename + '_V1_'
                            + str(np.round(V1,3)).replace('.','_') + 'V_V2_'
                            + str(np.round(V2,3))).replace('.','_') + 'V'
            AGmeasure(paramFile=paramFile, filename=thisFilename)

def grabInputArgs():
    parser = argparse.ArgumentParser(
                        description='This script sends two tones'
                                    'using Moku and reads spectrum using'
                                    'AG4395A.'
    )
    parser.add_argument('paramFile', nargs='?',
                        help='The parameter file for the measurement in'
                             'AG4395A.',
                        default=None)
    parser.add_argument('-f', '--filename', help='Stem of output filename.'
                        'Overrides parameter file.', default=None)
    parser.add_argument('--f1', type=float, help='Tone 1 Frequency.', default=37e6)
    parser.add_argument('--f2', type=float, help='Tone 2 Frequency.', default=73e6)
    return parser.parse_args()

if __name__ == "__main__":
    args = grabInputArgs()
    twoToneTest(args.paramFile, args.filename, args.f1, args.f2)
