'''
bnvsFSSgains
This script will take a beatnote timeseries measurement usng Moku on
triggerTime everyday. Then it will run mokuReadFreqNoise and will also create
a log file containing experiment configuration details in yaml format.
Finally, it will git add the three files generated and commit and push them.
'''

from mokuReadFreqNoise import mokuReadFreqNoise
from mokuPhaseMeterTimeSeries import mokuPhaseMeterTimeSeries
from measBNASDfromSR import measBNASDfromSR
from writeBNLog import writeLog
import argparse
import time
from epics import caget, caput
import os

BNFreqCH = "C3:PSL-PRECAV_BEATNOTE_FREQ"
gainCycleNCH = "C3:PSL-NCAV_FSS_FASTMON_RMS_GAINCYCLE_EN"
gainCycleSCH = "C3:PSL-SCAV_FSS_FASTMON_RMS_GAINCYCLE_EN"
requiredStates = ["C3:PSL-SCAV_FSS_RAMP_EN",
                  "C3:PSL-NCAV_FSS_RAMP_EN",
                  "C3:PSL-NCAV_PMC_SW2_IN",
                  "C3:PSL-SCAV_PMC_SW1_IN"]
NFSSCOMCH = "C3:PSL-NCAV_FSS_COMGAIN_DB"
NFSSFASTCH = "C3:PSL-NCAV_FSS_FASTGAIN_DB"
SFSSCOMCH = "C3:PSL-SCAV_FSS_COMGAIN_DB"
SFSSFASTCH = "C3:PSL-SCAV_FSS_FASTGAIN_DB"


def main(args):
    if args.detector == 'SN101':
        BNuppLimit = 37.34   # MHz
        BNlowLimit = 17.34   # MHz
    else:
        BNuppLimit = 120     # MHz
        BNlowLimit = 0       # MHz
    dataFileName = None

    iniNFSSCOM = caget(NFSSCOMCH)
    iniNFSSFAST = caget(NFSSFASTCH)
    iniSFSSCOM = caget(SFSSCOMCH)
    iniSFSSFAST = caget(SFSSFASTCH)

    step = 0
    while(step < args.nos):
        NFSSCOM = args.NFSSCOM_Start + step*args.NFSSCOM_Step
        NFSSFAST = args.NFSSFAST_Start + step*args.NFSSFAST_Step
        SFSSCOM = args.SFSSCOM_Start + step*args.SFSSCOM_Step
        SFSSFAST = args.SFSSFAST_Start + step*args.SFSSFAST_Step

        caput(NFSSCOMCH, NFSSCOM)
        caput(NFSSFASTCH, NFSSFAST)
        caput(SFSSCOMCH, SFSSCOM)
        caput(SFSSFASTCH, SFSSFAST)

        altFileRoot = ('BN_NCOM_' + str(round(NFSSCOM))
                       + '_NFAST_' + str(round(NFSSFAST))
                       + '_SCOM_' + str(round(SFSSCOM))
                       + '_SFAST_' + str(round(SFSSFAST)))
        print('Attempting beatnote spectrum measurement with:')
        print('NFSS COM Gain:', round(NFSSCOM), 'dB')
        print('NFSS FAST Gain:', round(NFSSFAST), 'dB')
        print('SFSS COM Gain:', round(SFSSCOM), 'dB')
        print('SFSS FAST Gain:', round(SFSSFAST), 'dB')

        # Wait for the FSS loops to stabalize
        time.sleep(5)

        # Start attempting to take beatnote spectrum
        attemptNo = 1
        dataFileName = None
        while ((dataFileName is None)
               and (attemptNo < 10)
               and ANDChannels(requiredStates)):
            print('Attempt No ', attemptNo)
            currBN = caget(BNFreqCH)
            withinLimits = ((currBN < BNuppLimit)
                            and (currBN > BNlowLimit))
            if withinLimits:
                if args.instrument == 'moku':
                    dataFileName = mokuPhaseMeterTimeSeries(
                                        chan='ch1',
                                        ipAddress=args.ipAddress,
                                        duration=args.duration,
                                        sampleRate=args.sampleRate,
                                        bandWidth=args.bandWidth,
                                        fiftyr=True, atten=False, ac=False,
                                        altFileRoot=altFileRoot + '_TS',
                                        useExternal=True, useSD=False,
                                        fileType='bin')
                    if dataFileName != 'Error':
                        print('Converting binary file into csv.')
                        os.system('~/Git/lireader/liconvert '
                                  + dataFileName)
                        os.remove(dataFileName)
                        dataFileName = dataFileName.replace('.li', '.csv')
                        print(dataFileName + ' created.')
                elif args.instrument == 'sr785':
                    dataFileName = measBNASDfromSR(
                                            args.paramFile,
                                            fileRoot=altFileRoot + '_Spectrum')
                    asdFileName = dataFileName
            # If BN frequency was in limits and measurement was succesful
            if dataFileName != 'Error' and withinLimits:
                print('Measurement succesful! Calculating ASD...')
                if args.instrument == 'moku':
                    asdFileName = dataFileName.replace('.csv', '.txt')
                    asdFileName = asdFileName.replace('_TS',
                                                      '_Spectrum')
                    mokuReadFreqNoise(dataFileName=dataFileName,
                                      asdFileName=asdFileName)
                print('Saving Experiment Configuration...')
                logFileName = writeLog(asdFileName, args.chListFile,
                                       args.detector, args.instrument)
                step = step + 1
                print('Done')
            # Measurement was not succesful. Wait for 5s and try again.
            else:
                if not withinLimits:
                    print('Beatnote frequency found to be',
                          currBN, 'MHz is outside detection range.')
                    if dataFileName != 'Error':
                        if dataFileName is not None:
                            os.remove(dataFileName)
                print('Trying again in 5 seconds ...')
                dataFileName = None
                attemptNo = attemptNo + 1
                time.sleep(5)
        if not ANDChannels(requiredStates):
            print('Required States not met.')

    caput(NFSSCOMCH, iniNFSSCOM)
    caput(NFSSFASTCH, iniNFSSFAST)
    caput(SFSSCOMCH, iniSFSSCOM)
    caput(SFSSFASTCH, iniSFSSFAST)


# find boolean AND of a list of binary EPICs channels
def ANDChannels(chanList):
    return all([caget(ii) for ii in chanList])


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This scrip changes FSS gains and takes beatnote spectrum '
                    'to see if any noise change happens in the experiment.')
    parser.add_argument('--instrument', type=str,
                        help='Moku or SR785',
                        default='Moku')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku or SR785',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is veryfast.',
                        default='veryfast')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('-c', '--chListFile', type=str,
                        help='ChannelList file with path to write in logs.',
                        default='ChannelList.txt')
    parser.add_argument('--paramFile', nargs='?',
                        help='The parameter file for the measurement by SR785',
                        default='CTN_BNSpec_SR785.yml')
    parser.add_argument('--NFSSCOM_Start', type=int,
                        help='Starting COMMON Gain value of NFSS in dB.'
                             'Default is 14.',
                        default=14)
    parser.add_argument('--NFSSFAST_Start', type=int,
                        help='Starting FAST Gain value of NFSS in dB.'
                             'Default is 14.',
                        default=14)
    parser.add_argument('--SFSSCOM_Start', type=int,
                        help='Starting COMMON Gain value of SFSS in dB.'
                             'Default is 24.',
                        default=24)
    parser.add_argument('--SFSSFAST_Start', type=int,
                        help='Starting FAST Gain value of SFSS in dB.'
                             'Default is 18.',
                        default=18)
    parser.add_argument('--NFSSCOM_Step', type=int,
                        help='Stepping COMMON Gain value of NFSS in dB.'
                             'Default is -3.',
                        default=-3)
    parser.add_argument('--NFSSFAST_Step', type=int,
                        help='Stepping FAST Gain value of NFSS in dB.'
                             'Default is -3.',
                        default=-3)
    parser.add_argument('--SFSSCOM_Step', type=int,
                        help='Stepping COMMON Gain value of SFSS in dB.'
                             'Default is -3.',
                        default=-3)
    parser.add_argument('--SFSSFAST_Step', type=int,
                        help='Stepping FAST Gain value of SFSS in dB.'
                             'Default is -3.',
                        default=-3)
    parser.add_argument('--nos', type=int,
                        help='Number of steps in FSS gain stepping.'
                             'Default is 5.',
                        default=5)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    args.instrument = args.instrument.lower()
    if args.instrument != 'moku' and args.instrument != 'sr785':
        raise RuntimeError('Instrument can be Moku or SR785 only.')
    main(args)
