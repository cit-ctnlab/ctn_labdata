This folder contains active probe measurments of test points on south path (serial 2009:007) TTFSS box servo board. This was measured on october 19, 2017.

Input power was injected using BNC to test clip at TP1 on D090105 (Rev. C) at -20 dBm.  Previously measuments made with 0 dBm injected had saturated all stages after stage 2 op amp.  

Active probe used was  HP 41800a and network analyzer was HP4395A over the interval 1 kHz to 100 MHz (1000 points) with 300 Hz IF bandwidth.
