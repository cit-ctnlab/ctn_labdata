This folder contains transfer functions of the north and south FSS boxes RF filters after tuning their notches to match the modulation frequency.  

The default value of 21.5 MHz was not modified for the 14.75 MHz modulation and was not right for the 36 and 37 MHz modulation either.  

For the North TTFSS field box L3 was changed from 1.2 uH to 750 nH and C12 was modified to 22 + 1.5 +1.5 +1.5 pF. This gave a notch frequency  of pretty much exactly 36 MHz.  A transfer function was taken from 1 MHz out to 60 MHz as well as a zoomed in TF around 36 MHz with a span of 4 MHz.  

After this North RF board capacitors C13 and C14 were changed from 220 pF to 1000 pF to lower the poll of the LP and increase the RF attenution further.
