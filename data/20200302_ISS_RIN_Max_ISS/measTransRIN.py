from SRmeasure import main as SRmeasure
from epics import caget
import os
import numpy as np

STransDCCH = 'C3:PSL-SCAV_TRANS_DC'
NTransDCCH = 'C3:PSL-NCAV_TRANS_DC'

for ii in range(500):
    SDCval = caget(STransDCCH)
    NDCval = caget(NTransDCCH)
    header = ('North DC Val: ' + str(NDCval) + ' Volts'
              + 'South DC Val: ' + str(SDCval) + ' Volts')
    fn1 = SRmeasure(paramFile='TransSpec1.yml',
                    extraHeader=header)
    SDCval = caget(STransDCCH)
    NDCval = caget(NTransDCCH)
    header = ('North DC Val: ' + str(NDCval) + ' Volts'
              + 'South DC Val: ' + str(SDCval) + ' Volts')
    fn2 = SRmeasure(paramFile='TransSpec2.yml',
                    extraHeader=header)
    with open(fn1, 'r') as f:
        fn1lines = f.readlines()
    with open(fn2, 'r') as f:
        fn2lines = f.readlines()
    allLines = (fn1lines
                + ['#\n', '#\n']
                + fn2lines[:31]
                + fn2lines[38:])
    os.remove(fn1)
    os.remove(fn2)
    with open(fn1, 'w') as f:
        f.writelines(allLines)
