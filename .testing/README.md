This folder contains elements essential for gitlab CI testing and deployment.  It is a only 'hidden' for default view to keep it out of the way and creating clutter.

This is mostly a place to put bash and python scripts used to run tests on things commited to the noise budget code.  

There are also scripts for checking commited notebooks to make sure users aren't commiting junk in the output
